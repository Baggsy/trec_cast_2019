import argparse
import torch
from eval_helper_fns import generate_fake_qrels_file, get_eval_score_and_runs, generate_trec_cast_file_from_model_run
from pytorch_pretrained_bert import BertTokenizer
from utils import init_seed_get_device
import os
import time


def test(config):
    """
    Performs the evaluation test for on epoch loading a trained model
    :param config: the configuration argument set
    """

    # Setup output file to log info in
    output_filename = os.path.join(config.output_dir, config.output_log_filename)
    output_file = open(output_filename, 'w')
    output_file.write(str(config))
    output_file.write("\n")

    t0 = time.time()
    print("\nconfig.distributed_launch: {}".format(config.distributed_launch))
    print("config.output_dir: {}".format(config.output_dir))
    print("config.model_file: {}\n".format(config.model_file))
    device_name, using_cuda = init_seed_get_device(config)

    # Load trained model and tokeniser
    tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
    using_cuda = True if using_cuda and config.using_cuda else False
    checkpoint = torch.load(config.model_file, map_location=device_name if using_cuda else 'cpu')
    output_file.write("Model config: {}\n".format(checkpoint['config']))
    output_file.write("Model epoch: {}\n".format(checkpoint['epoch']))
    model = torch.load(config.model_file+".model", map_location=device_name if using_cuda else 'cpu')
    output_file.write("train_losses: {}\n".format(checkpoint['train_losses']))
    del checkpoint

    # Load qrels file if given or create a temporary one for the TEST set
    if config.qrels is None:
        tmp_qrels_filename = generate_fake_qrels_file(config)
        qrels_file = tmp_qrels_filename
    else:
        qrels_file = config.qrels

    # Calcualte the metrics' scores for this model
    mean_metrics_dict, eval_runs = get_eval_score_and_runs(pretrain_marco=config.pretrain_marco, tokenizer=tokenizer,
                                                           using_cuda=using_cuda,
                                                           run_file=config.run_file,
                                                           max_docs=config.max_docs, qrels=qrels_file,
                                                           batch_size=config.batch_size,
                                                           num_workers=config.num_workers, model=model,
                                                           metrics=config.metrics,
                                                           max_num_queries=config.max_num_queries,
                                                           eval_query_text_file=config.eval_query_text_file,
                                                           anserini_index_path=config.anserini_index_path,
                                                           do_normalize_initial_score=config.do_normalize_initial_score,
                                                           combine_normalised_scores=config.combine_normalised_scores,
                                                           is_QUAC=config.is_QUAC)

    print(mean_metrics_dict)
    output_file.write("For filename: {}\n".format(config.model_file))
    output_file.write("Evaluation time: {:.2f}m\n".format((time.time() - t0) / 60))

    # Generate run in trec cast format for submission to the TREC CAsT challenge
    os.makedirs(config.output_dir, exist_ok=True)
    generate_trec_cast_file_from_model_run(eval_runs, os.path.join(config.output_dir, config.output_run_filename),
                                           config.max_docs, config.identifier,
                                           config.pretrain_marco)
    # Log scores
    if config.qrels is None:
        os.remove(tmp_qrels_filename)
        output_file.write('No evaluation scores (qrels not available)\n')
    else:
        opt_metric_score = mean_metrics_dict[config.optimisation_metric]
        print("Score ({}): {}".format(config.optimisation_metric, opt_metric_score))
        print(mean_metrics_dict)
        for metric in sorted(config.metrics):
            output_file.write("{}: {:.4f}\n".format(metric, mean_metrics_dict[metric]))
        output_file.write("Maximum memory cached for the whole duration of the experiment: {}\n".format(torch.cuda.max_memory_cached()))
        output_file.write(str(opt_metric_score)+"\n")
    output_file.write("EOF")
    output_file.close()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--seed", type=int, default=42, help="the random seed for reproducability")
    parser.add_argument("--local_rank", type=int, default=0, help="the local rank in case of distributed launch")
    parser.add_argument('--world-size', type=int, default=1, help="the world size for distributed launch")
    parser.add_argument("--model_file", type=str, required=True, help="the model file containing the trained weights")
    parser.add_argument("--output_dir", type=str, required=True, help="the directory to save the output files")
    parser.add_argument("--output_run_filename", type=str, required=True,
                        help="the name of the out run in trec cast format")
    parser.add_argument("--run_file", type=str, required=True, help="the ranked list run file")
    parser.add_argument("--eval_query_text_file", type=str, help="the dialogue text file")
    parser.add_argument("--anserini_index_path", type=str, help="the path to the anserini index of the collections")
    parser.add_argument("--optimisation_metric", type=str, default="recip_rank", help="the otpimisation metric to use")
    parser.add_argument("--batch_size", type=int, default=32, help="the batch size for evaluation")
    parser.add_argument("--num_workers", type=int, default=0, help="the number of workers for dataloading")
    parser.add_argument("--verbosity", type=int, default=1, help="the verbosity level for debugging")
    parser.add_argument("--metrics", type=str, default='map,ndcg,recip_rank', help="the metrics to score")
    parser.add_argument("--qrels", type=str, help='if not given , using fake qrels.')
    parser.add_argument("--identifier", type=str, required=True,
                        help="the identifier name to use for the run required for the TREC CAsT challenge")
    parser.add_argument("--max_docs", type=int, default=1000, required=True,
                        help="The number of maximum documents to use for the ranked list")
    parser.add_argument("--max_num_queries", type=int, default=500, required=True,
                        help="the maximum number of queries to use for the evaluation score. More queries gives higher"
                             "fidelity score but takes longer")
    parser.add_argument("--no_distributed_launch", dest="distributed_launch", action='store_false',
                        help="whether the file is running with a distributed launch")

    parser.add_argument("--pretrain_marco", dest="pretrain_marco", action="store_true",
                        help="whether using to evaluate on the MARCO top1000 dev set")
    parser.add_argument('--cuda', dest="using_cuda", action="store_true", help="whther to use the GPU")
    parser.add_argument("--output_log_filename", type=str, required=True,
                        help="the name of the file to write the log information in")

    parser.add_argument("--do_normalize_initial_score", dest="do_normalize_initial_score", action='store_true',
                        help="whether to normalise the initial score during evaluation")
    parser.add_argument("--combine_normalised_scores", dest="combine_normalised_scores", action='store_true',
                        help="whether to combine the un-normalised with the normalised scores")

    parser.add_argument("--is_QUAC", dest="is_QUAC", action="store_true", help="whether evaluating on the QuAC dataset")

    args = parser.parse_args()
    args.metrics = set(args.metrics.split(','))
    if not args.pretrain_marco:
        assert args.eval_query_text_file is not None
        assert args.anserini_index_path is not None

    test(args)


if __name__ == '__main__':
    main()
