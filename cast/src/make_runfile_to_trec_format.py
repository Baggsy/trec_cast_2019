import argparse
from collections import defaultdict

from eval_helper_fns import generate_trec_cast_file_from_model_run


def main():
    """
    Makes a run file into the TREC CAsT run format for competing in the challenge
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--run_file", type=str, required=True)
    parser.add_argument("--max_docs", type=int, default=1000)
    parser.add_argument("--identifier", type=str, required=True)
    parser.add_argument("--pretraining", dest="was_pretraining", action="store_true")
    parser.add_argument("--output_file", type=str, required=True)
    args = parser.parse_args()

    if len(args.identifier) > 15:
        print("Identifier for run must be below 15 characters. Identifier '{}' is {} long".format(args.identifier, len(args.identifier)))
        assert len(args.identifier) <= 15

    trec_run_filename = args.output_file if args.output_file[:-4] == "trec" else args.output_file + ".trec"
    print("Converting file {} into trec format at {}".format(args.run_file, trec_run_filename))
    run_file = open(args.run_file, "r")

    run = defaultdict(dict)
    for line in run_file:
        q_id, _, d_id, rank, score, _ = line.split(' ')
        run[q_id][d_id] = score
    generate_trec_cast_file_from_model_run(run, run_output_filename=trec_run_filename, max_docs=args.max_docs,
                                           identifier=args.identifier, pretrain_marco=args.was_pretraining)


if __name__ == '__main__':
    main()
