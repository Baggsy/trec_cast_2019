import json
import os
import random
from collections import defaultdict
from joblib import dump, load

import torch
from torch.nn.utils.rnn import pad_sequence
from torch.utils.data import Dataset

from dataset.tools import read_query_text_dict, read_run_dict, read_docs_text_dict, cached_tmp_path, \
    min_max_norm_run_dict, cap_to_top_docs, combine_norm_run_dict, correct_qid, un_correct_qid


class RankingDataset(Dataset):
    def __init__(self, qrels_file, runs_file, query_text_file, anserini_index_path, tokenizer, mode,
                 num_neg=1, using_cuda=False, do_normalize_initial_score=True, do_cap_to_top_docs=-1,
                 combine_normalised_scores=False, train_global_QUAC=False):
        """
        Constructs the ranking dataset for the TREC CAsT data
        :param qrels_file: the file containing the qrels
        :param runs_file: the runs file of the unsupervised language model ranking
        :param query_text_file: the dialogues text file
        :param anserini_index_path: the path to the anserini index of the collections
        :param tokenizer: the pre-trained tokeniser
        :param mode: the mode whether it is training('train') or evaluating('eval')
        :param num_neg: the number of negative passages to sample
        :param using_cuda: whether to use the GPU
        :param do_normalize_initial_score: whether to normalise the initial scores of the unsupervised language model
        :param do_cap_to_top_docs: the number of documents to use for the top k list. -1: use all documents from the run file
        :param combine_normalised_scores: whether to combine the un-normalised with the normalised scores as embeddingds
        :param train_global_QUAC: whether to use the global features' embeddingds for the QuAC dataset
        """
        super(RankingDataset, self).__init__()
        self.using_cuda = using_cuda

        self.mode = mode
        self.num_neg = num_neg
        self.train_global_QUAC = train_global_QUAC

        self.do_normalize_initial_score = do_normalize_initial_score
        self.do_cap_to_top_docs = do_cap_to_top_docs
        self.combine_normalised_scores = combine_normalised_scores

        self.data_dict = self.qid_str2int = self.docid_str2int = self.qid_int2str = self.docid_int2str = \
            self.qrels_dict = None

        # Read the qrels
        self._init_qrels_dict(qrels_file)

        # Read the runs and the dialogues
        self._init_data_dict(tokenizer, runs_file, query_text_file, anserini_index_path)

    def _init_qrels_dict(self, qrels_file):
        """
        Load the qrels file
        """
        self.qrels_dict = json.load(open(qrels_file))

    def _init_data_dict(self, tokenizer, runs_file, query_text_file, anserini_index_path):
        """
        Initialise the data dictionary loading the runs and the dialogues files
        :param tokenizer: pre-trained tokeniser
        :param runs_file: the runs file from the unsupervised language model
        :param query_text_file: the dialogues text
        :param anserini_index_path: the path to the anserini index of the collections
        :return: the data dictionary
        """
        # Define a name for the anserini run for caching
        self.anserini_run_name = os.path.basename(os.path.normpath(runs_file)).replace('.run', '') \
                                 + "_" + str(self.combine_normalised_scores) \
                                 + "_" + str(self.do_normalize_initial_score) \
                                 + "_" + str(self.train_global_QUAC)

        # Read runs file
        self.run_dict = read_run_dict(runs_file, self.train_global_QUAC)

        # Checking for top k limit of ranked list
        if not self.do_cap_to_top_docs == -1:
            self.run_dict = cap_to_top_docs(self.run_dict, self.do_cap_to_top_docs)

        if self.combine_normalised_scores:
            self.run_dict = combine_norm_run_dict(self.run_dict)
        elif self.do_normalize_initial_score:
            self.run_dict = min_max_norm_run_dict(self.run_dict)

        # Read the dialogues
        self.query_text_dict = read_query_text_dict(query_text_file, tokenizer, merge_text=True, is_QUAC=self.train_global_QUAC)
        self.doc_text_dict = read_docs_text_dict(anserini_index_path, self.anserini_run_name, self.run_dict, tokenizer)
        self.create_query_doc_ids_list(self.mode, self.qrels_dict, self.run_dict)

    def __getitem__(self, index):
        """
        Returns the item at a certain index in the data triples pairs (for training) or labeled scores (for evaluation)
        """
        attr_keys = ['qid', 'q_text', 'docid', 'doc_text', 'q_turn']
        if self.mode == 'train':
            attr_keys.extend(['neg_docid', 'neg_doc_text', 'initial_score_pos', 'initial_score_neg'])
        else:
            attr_keys.extend(['label', 'initial_score'])

        instance = {k: self.data_dict[k][index]
                    for k in attr_keys}
        return instance

    def __len__(self):
        num_instances = len(self.data_dict['q_text'])
        return num_instances

    def get_qrels_dict(self):
        return self.qrels_dict

    def create_query_doc_ids_list(self, mode, qrels_dict, run_dict):
        """
        Create ids for all the individual queries for indexing
        """
        self.data_dict = defaultdict(list)
        self.qid_str2int = dict()
        self.docid_str2int = dict()
        print('initializing {} dataset...'.format(mode))

        if mode == 'train':
            self.create_train_query_doc_ids_list(qrels_dict, run_dict)
        else:
            self.create_eval_query_doc_ids_list(qrels_dict, run_dict)

        self.qid_int2str = {v: k for k, v in self.qid_str2int.items()}
        self.docid_int2str = {v: k for k, v in self.docid_str2int.items()}
        self.print_stats()

    def print_stats(self):
        print(self.mode)
        print('\t# queries: {}'.format(len(self.qid_str2int)))
        print('\t# docs: {}'.format(len(self.docid_str2int)))

        if self.mode == 'train':
            print('\t# query pos/neg doc pairs: {}'.format(self.__len__()))
        else:
            print('\t# query/doc pairs: {}'.format(self.__len__()))

    @staticmethod
    def _split_query_rel_docs_per_label(qrels_dict, qid, q_run):
        rel_docs_labels = qrels_dict.get(qid)

        if not rel_docs_labels:
            # ignore qids for which we don't have a relevant doc in the qrels
            return None, 0

        rel_docs_per_label = defaultdict(set)
        for rel_doc, label in rel_docs_labels.items():
            if rel_doc not in q_run:
                continue
            rel_docs_per_label[label].add(rel_doc)

        if not rel_docs_per_label:
            return None, 0

        max_label = max(rel_docs_per_label.keys())

        return rel_docs_per_label, max_label

    def create_train_query_doc_ids_list(self, qrels_dict, run_dict):
        # generates pairwise data
        # handles multiple labels
        # ignores queries for which we don't have a relevant doc in the run.
        num_non_rel_docs_per_qid = 0
        print("len(run_dict): {}".format(len(run_dict)))
        for qid, q_run in run_dict.items():

            rel_docs_per_label, max_label = self._split_query_rel_docs_per_label(qrels_dict, un_correct_qid(qid, self.train_global_QUAC), q_run)
            if rel_docs_per_label is None or max_label == 0:
                print('No relevant documents retrieved for qid: {}'.format(qid))
                print("rel_docs_per_label: {}".format(rel_docs_per_label))
                print("max_label: {}".format(max_label))
                num_non_rel_docs_per_qid += 1
                continue

            retrieved_docs = set(q_run.keys())
            for label in reversed(range(1, max_label+1)):
                rel_docs_label = rel_docs_per_label[label]
                neg_docs_pool = retrieved_docs.difference(rel_docs_label)
                for rel_doc in rel_docs_label:
                    initial_score_pos = q_run[rel_doc]
                    sampled_neg_docs = random.sample(neg_docs_pool, self.num_neg)
                    for neg_doc in sampled_neg_docs:
                        initial_score_neg = q_run[neg_doc]
                        self.add_single_instance_to_data_train(qid, rel_doc, neg_doc,
                                                               initial_score_pos, initial_score_neg)

        print("\n----------------\n num_non_rel_docs_per_qid: {}\n--------------\n".format(num_non_rel_docs_per_qid))

    def create_eval_query_doc_ids_list(self, qrels_dict, run_dict):
        for qid, q_run in run_dict.items():
            for docid, initial_score in q_run.items():
                try:
                    label = qrels_dict[un_correct_qid(qid, self.train_global_QUAC)][docid]
                except KeyError:
                    label = 0
                self.add_single_instance_to_data_eval(qid, docid, label, initial_score)

    def add_single_instance_to_data_common(self, qid_str, docid_str):
        self.add_qid_if_not_exists(qid_str)
        self.add_docid_if_not_exists(docid_str)

        max_num_questions = 13 #TODO change to parameter
        assert int(qid_str.split('_')[-1]) < max_num_questions
        q_turn = _convert_to_torch_var_long(int(qid_str.split('_')[-1]))
        q_turn = torch.eye(max_num_questions)[q_turn].squeeze()
        self.data_dict['q_turn'].append(q_turn)

        qid_int = _convert_to_torch_var_long(self.get_int_qid(qid_str))
        self.data_dict['qid'].append(qid_int)

        q_text = self.query_text_dict[qid_str]
        q_text = _convert_to_torch_var_long(q_text, is_list=True)
        self.data_dict['q_text'].append(q_text)

        docid_int = _convert_to_torch_var_long(self.get_int_docid(docid_str))
        self.data_dict['docid'].append(docid_int)

        doc_text = self.doc_text_dict[docid_str]
        doc_text = _convert_to_torch_var_long(doc_text, is_list=True)
        self.data_dict['doc_text'].append(doc_text)

    def add_single_instance_to_data_eval(self, qid_str, docid_str, label, initial_score):
        self.add_single_instance_to_data_common(qid_str, docid_str)

        label = _convert_to_torch_var_long(label)
        self.data_dict['label'].append(label)

        initial_score = _convert_to_torch_var_float(initial_score)
        self.data_dict['initial_score'].append(initial_score)

    def add_single_instance_to_data_train(self, qid_str, positive_docid_str, negative_docid_str,
                                          initial_score_pos, initial_score_neg):
        self.add_single_instance_to_data_common(qid_str, positive_docid_str)

        self.add_docid_if_not_exists(negative_docid_str)

        neg_docid_int = _convert_to_torch_var_long(self.get_int_docid(negative_docid_str))
        self.data_dict['neg_docid'].append(neg_docid_int)

        neg_doc_text = self.doc_text_dict[negative_docid_str]
        neg_doc_text = _convert_to_torch_var_long(neg_doc_text, is_list=True)
        self.data_dict['neg_doc_text'].append(neg_doc_text)

        initial_score_pos = _convert_to_torch_var_float(initial_score_pos)
        self.data_dict['initial_score_pos'].append(initial_score_pos)

        initial_score_neg = _convert_to_torch_var_float(initial_score_neg)
        self.data_dict['initial_score_neg'].append(initial_score_neg)

    def get_int_qid(self, id_str):
        return self.qid_str2int[id_str]

    def get_int_docid(self, id_str):
        return self.docid_str2int[id_str]

    def add_qid_if_not_exists(self, id_str):
        self._add_id_if_not_exists(id_str, self.qid_str2int)

    def add_docid_if_not_exists(self, id_str):
        self._add_id_if_not_exists(id_str, self.docid_str2int)

    @staticmethod
    def _add_id_if_not_exists(id_str, _dict):
        if id_str not in _dict:
            _dict[id_str] = len(_dict)

    def get_real_q_id(self, int_id):
        return self.qid_int2str[int_id]

    def get_real_p_id(self, int_id):
        return self.docid_int2str[int_id]

    def collate_fn(self, data):
        def _pad_seq(x):
            return pad_sequence(x, batch_first=True)

        attrs = ['qid', 'q_text', 'docid', 'doc_text', 'q_turn']
        if self.mode == 'train':
            attrs.append('neg_docid')
            attrs.append('neg_doc_text')
            attrs.append('initial_score_pos')
            attrs.append('initial_score_neg')
        else:
            attrs.append('label')
            attrs.append('initial_score')

        batch_dict = defaultdict(list)
        for instance in data:
            for key in attrs:
                batch_dict[key].append(instance[key])

        for key in attrs:
            if 'text' in key:
                process_fn = _pad_seq
            else:
                process_fn = torch.stack
            batch_dict[key] = process_fn(batch_dict[key])

        return dict(batch_dict)

    @staticmethod
    def cache_dict(file_name, dict_to_save):
        cached_filepath = os.path.join(cached_tmp_path, 'datadict_{}.pickle'.format(file_name))
        if os.path.exists(cached_filepath):
            return
        print('caching {} dict...'.format(file_name))
        dump(dict_to_save, cached_filepath)
        print('done.')

    @staticmethod
    def load_cached_dict(file_name):
        cached_filepath = os.path.join(cached_tmp_path, 'datadict_{}.pickle'.format(file_name))
        if not os.path.exists(cached_filepath):
            return False

        print('loading cached {} dict...'.format(file_name))
        return load(cached_filepath)


def _convert_to_torch_var_long(_x, is_list=False):
    if not is_list:
        _x = [_x]
    return torch.tensor(_x, requires_grad=False).long()


def _convert_to_torch_var_float(_x):
    return torch.tensor([_x], requires_grad=False).float()
