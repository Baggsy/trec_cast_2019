import torch.nn as nn
from utils import *


class EmbAggEncoder(nn.Module):
    """
    Simple embedding encoder
    """
    def __init__(self, vocab_size, embedding_size, drop_prob=0.5):
        super(EmbAggEncoder, self).__init__()
        self.embedding = nn.Embedding(vocab_size, embedding_size, padding_idx=0)
        self.dropout = nn.Dropout(drop_prob)

    def forward(self, s):
        s_mask = s.ne(0).detach()
        s_mask = s_mask.unsqueeze(-1).float()
        emb_s = self.dropout(self.embedding(s))
        emb_s *= s_mask
        return torch.sum(emb_s, dim=-2)

