import random
from collections import defaultdict
import gzip
from dataset.ranking_dataset import RankingDataset, _convert_to_torch_var_long, _convert_to_torch_var_float


class PretrainingTriplesDataset(RankingDataset):
    """
    Dataset used specifically for the MS MARCO triples pairs
    """
    def __init__(self, triples_text_file, tokenizer, max_num_docs=10000, using_cuda=False):
        self.fake_id = 0
        self.max_num_tokens = 512
        self.max_num_docs = max_num_docs
        self.dataset_name = 'MARCOTriples_{}'.format(max_num_docs)

        super(PretrainingTriplesDataset, self). \
            __init__(None, triples_text_file, None, None, tokenizer, 'train',
                     num_neg=1, using_cuda=using_cuda)
        if max_num_docs <= 500000: # Limit due to caching size
            self.cache_dict(self.dataset_name, self.data_dict)

    def _init_qrels_dict(self, _):
        self.qrels_dict = None

    def _init_data_dict(self, tokenizer, triples_text_file, _x1, _x2):
        self.construct_data_dict(triples_text_file, tokenizer)

    def get_new_fake_id(self):
        tmp = self.fake_id
        self.fake_id += 1
        return tmp

    def construct_data_dict(self, triples_text_file, tokenizer):
        self.data_dict = self.load_cached_dict(self.dataset_name)
        if self.data_dict is not False:
            return

        self.data_dict = defaultdict(list)
        ignored_counter = 0
        print('Initializing <={} training triples...'.format(self.max_num_docs))

        for n_file, line in enumerate(gzip.open(triples_text_file, 'rt')):
            try:
                query, positive_passage, negative_passage = line.split('\t')
            except ValueError:
                ignored_counter += 1
                continue

            qid_int = self.get_new_fake_id()
            docid_int = self.get_new_fake_id()
            neg_docid_int = self.get_new_fake_id()

            # dummy values
            q_turn = _convert_to_torch_var_long(qid_int)
            self.data_dict['q_turn'].append(q_turn)
            self.data_dict['qid'].append(_convert_to_torch_var_long(qid_int))
            self.data_dict['docid'].append(_convert_to_torch_var_long(docid_int))
            self.data_dict['neg_docid'].append(_convert_to_torch_var_long(neg_docid_int))

            self.data_dict['q_text'].append(_convert_to_torch_var_long(
                tokenizer.convert_tokens_to_ids(tokenizer.tokenize(query)[:self.max_num_tokens]), is_list=True))
            self.data_dict['doc_text'].append(_convert_to_torch_var_long(
                tokenizer.convert_tokens_to_ids(tokenizer.tokenize(positive_passage)[:self.max_num_tokens]), is_list=True))
            self.data_dict['initial_score_pos'].append(_convert_to_torch_var_float(.0))  # dummy
            self.data_dict['neg_doc_text'].append(_convert_to_torch_var_long(
                tokenizer.convert_tokens_to_ids(tokenizer.tokenize(negative_passage)[:self.max_num_tokens]), is_list=True))
            self.data_dict['initial_score_neg'].append(_convert_to_torch_var_float(.0))  # dummy

            if (n_file + 1) == self.max_num_docs:
                break

        self.data_dict = dict(self.data_dict)
        print('Loaded {} training triples'.format(len(self.data_dict['qid'])))
        print('WARNING: Ignored {} triples.'.format(ignored_counter))


class MARCODevDataset(RankingDataset):
    """
    Separate evaluation dataset for the MS MARCO top1000 dev set
    """
    def __init__(self, marco_dev_file, marco_qrels_file, tokenizer, max_num_docs=10000, max_num_queries=500, using_cuda=False):
        self.fake_id = 0
        self.max_num_tokens = 512
        self.max_num_docs = max_num_docs
        self.max_num_queries = max_num_queries
        self.dataset_name = 'MARCODevDataset_{}'.format(self.max_num_queries)
        self.qrels_name = 'MARCODevQrels_{}'.format(self.max_num_queries)
        self.qid_int2str_filename = 'MARCODevQid_int2str_{}'.format(self.max_num_queries)
        self.docid_int2str_filename = 'MARCODevDocid_int2str_{}'.format(self.max_num_queries)
        self.qid_str2int_filename = 'MARCODevQid_str2int_{}'.format(self.max_num_queries)
        self.docid_str2int_filename = 'MARCODevDocid_str2int_{}'.format(self.max_num_queries)
        self.sample_qrels = True

        super(MARCODevDataset, self). \
            __init__(marco_qrels_file, marco_dev_file, None, None, tokenizer, 'eval',
                     num_neg=1, using_cuda=using_cuda)

        self.cache_dict(self.dataset_name, self.data_dict)
        self.cache_dict(self.qrels_name, self.qrels_dict)
        self.cache_dict(self.qid_int2str_filename, self.qid_int2str)
        self.cache_dict(self.docid_int2str_filename, self.docid_int2str)
        self.cache_dict(self.qid_str2int_filename, self.qid_str2int)
        self.cache_dict(self.docid_str2int_filename, self.docid_str2int)

    def _init_qrels_dict(self, qrels_file):
        self.qrels_dict = self.load_cached_dict(self.qrels_name)
        if self.qrels_dict is not False:
            self.sample_qrels = False
            return

        # 1102432	0	2026790	1
        self.qrels_dict = defaultdict(dict)
        with open(qrels_file) as fin:
            for line in fin:
                qid_str, _, did_str, label = line.split('\t')
                self.qrels_dict[qid_str][did_str] = int(label)

    def _init_data_dict(self, tokenizer, marco_dev_file, query_text_file, anserini_index_path):
        self.construct_data_dict(marco_dev_file, tokenizer)

    def subsample_qrels(self, marco_dev_file):
        valid_qids = set()
        # First get the valid qids
        for n_file, line in enumerate(gzip.open(marco_dev_file, 'rt')):
            if self.max_num_docs != -1 and (n_file + 1) > self.max_num_docs:
                break
            try:
                qid_str, _, _, _ = line.split('\t')
                valid_qids.add(qid_str)
            except ValueError:
                continue
            assert qid_str in self.qrels_dict

        # Select a subset of the queries for evaluation (since they are too many)
        sample_qids = random.sample(set(self.qrels_dict.keys()).intersection(valid_qids), self.max_num_queries)
        self.qrels_dict = {qid: v for qid, v in self.qrels_dict.items()
                           if qid in sample_qids}

    def construct_data_dict(self, marco_dev_file, tokenizer):
        self.data_dict = self.load_cached_dict(self.dataset_name)
        self.qid_int2str = self.load_cached_dict(self.qid_int2str_filename)
        self.docid_int2str = self.load_cached_dict(self.docid_int2str_filename)
        self.qid_str2int = self.load_cached_dict(self.qid_str2int_filename)
        self.docid_str2int = self.load_cached_dict(self.docid_str2int_filename)

        if self.data_dict is not False and self.qid_int2str is not False and self.docid_int2str is not False \
                and self.qid_str2int is not False and self.docid_str2int is not False:
            return

        self.data_dict = defaultdict(list)
        self.qid_str2int = dict()
        self.docid_str2int = dict()
        num_ignored_lines = 0

        print('reading dev qids..', marco_dev_file)

        if self.sample_qrels:
            self.subsample_qrels(marco_dev_file)
        print('# sample qids', len(self.qrels_dict))

        print('reading dev file', marco_dev_file)

        # Read texts
        for n_file, line in enumerate(gzip.open(marco_dev_file, 'rt')):

            if self.max_num_docs != -1 and (n_file + 1) > self.max_num_docs:
                break

            try:
                qid_str, docid_str, q_text, doc_text = line.split('\t')
            except ValueError:
                num_ignored_lines += 1
                continue

            if qid_str not in self.qrels_dict:
                continue

            self.add_qid_if_not_exists(qid_str)
            self.add_docid_if_not_exists(docid_str)

            # Dummy values
            q_turn = _convert_to_torch_var_long(self.get_int_qid(qid_str))
            self.data_dict['q_turn'].append(q_turn)

            self.data_dict['qid'].append(_convert_to_torch_var_long(self.get_int_qid(qid_str)))
            self.data_dict['docid'].append(_convert_to_torch_var_long(self.get_int_docid(docid_str)))
            self.data_dict['q_text'].append(_convert_to_torch_var_long(
                tokenizer.convert_tokens_to_ids(tokenizer.tokenize(q_text)[:self.max_num_tokens]), is_list=True))
            self.data_dict['doc_text'].append(_convert_to_torch_var_long(
                tokenizer.convert_tokens_to_ids(tokenizer.tokenize(doc_text)[:self.max_num_tokens]), is_list=True))
            self.data_dict['initial_score'].append(_convert_to_torch_var_float(.0))  # dummy

            label = int(self.qrels_dict[qid_str].get(docid_str, 0))
            self.data_dict['label'].append(_convert_to_torch_var_long(label))

        print('WARNING: Ignored {} lines from dev.'.format(num_ignored_lines))

        print('data dict len: {}'.format(len(self.data_dict['qid'])))
        self.qid_int2str = {v: k for k, v in self.qid_str2int.items()}
        self.docid_int2str = {v: k for k, v in self.docid_str2int.items()}
        self.print_stats()
