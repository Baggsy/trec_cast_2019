from models.abstract_model import AbstractModel
from models.cedr.rankers import VanillaBertRanker, CedrKnrmRanker
from models.encoders import EmbAggEncoder

import torch
import torch.nn as nn
import torch.nn.functional as F


class RepresentationModel(AbstractModel):
    """
    Simple NN model which uses only cosine similarity for sanity check whi
    """
    def __init__(self, vocab_size, embedding_size, device, drop_prob, eps=1e-10):
        super(RepresentationModel, self).__init__(device=device, eps=eps)

        self.encoder = EmbAggEncoder(vocab_size, embedding_size, drop_prob)

    def predict(self, query, document, initial_score, q_turn=None):
        q = self.encoder(query)
        d = self.encoder(document)

        score = torch.cosine_similarity(q, d)
        return score


class BERTModel(AbstractModel):
    """
    BERT ranker NN model implementing the vanilla BERT ranker from the CEDR paper
    """
    def __init__(self, device, ranker_name='vanilla', dropout_prob=0.1, apply_tanh_bert_output=False, eps=1e-10):
        super(BERTModel, self).__init__(device=device, eps=eps)
        self.apply_tanh_bert_output = apply_tanh_bert_output
        if self.apply_tanh_bert_output:
            self.tanh = torch.nn.Tanh()

        if ranker_name == 'vanilla':
            self.bert_ranker = VanillaBertRanker(dropout_prob)
        elif ranker_name.lower() == 'knrm':
            print("\nUsing KRNM ranker\n")
            self.bert_ranker = CedrKnrmRanker(dropout_prob)
        else:
            raise NotImplementedError

    def predict(self, query, document, initial_score, q_turn=None):
        out = self.bert_ranker.forward(query_tok=query, query_mask=None, doc_tok=document, doc_mask=None)
        if self.apply_tanh_bert_output:
            out = self.tanh(out)
        return out

    def fix_bert(self, verbosity=1):  # TODO where shall we use it?
        n_parameters_fixed = 0
        for name, param in self.bert_ranker.named_parameters():
            if 'bert' in name:
                if verbosity >= 2:
                    print('fix', name, param.size())
                n_parameters_fixed += 1
                param.requires_grad = False
        print("\nFixed {} parameters".format(n_parameters_fixed))

    def train_bert(self, verbosity=1):
        n_parameters_unfixed = 0
        for name, param in self.bert_ranker.named_parameters():
            if 'bert' in name:
                if verbosity >= 2:
                    print('train', name, param.size())
                n_parameters_unfixed += 1
                param.requires_grad = True
        print("\nUnfixed {} parameters".format(n_parameters_unfixed))


class BERTModelGlobalFeatures(BERTModel):
    """
    BERT model appending the global features as a linear layer on top of the BERT ranker output
    """
    def __init__(self, device, global_do_layer_norm=None, ranker_name='vanilla', dropout_prob=0.1, apply_tanh_bert_output=False, eps=1e-10, mask_q_turn=None, combine_normalised_scores=False):
        super(BERTModelGlobalFeatures, self).__init__(device, ranker_name=ranker_name,
                                  dropout_prob=dropout_prob, apply_tanh_bert_output=apply_tanh_bert_output, eps=eps)

        self.mask_q_turn = mask_q_turn
        self.tanh_bert = torch.nn.Tanh()
        self.combine_normalised_scores = combine_normalised_scores
        # TODO change 15 to parameter #num questions + initial score + quer-doc score
        n_input_linear = 15 if self.mask_q_turn is None else self.mask_q_turn + 2
        if combine_normalised_scores:
            n_input_linear += 1
        self.linear = nn.Linear(n_input_linear, 1)

    def predict(self, query, document, initial_score, q_turn):
        # query.shape: torch.Size([2, 19])
        # document.shape: torch.Size([2, 86])
        # initial_score.shape: torch.Size([2, 1])
        # q_turn.shape: torch.Size([2, 13])

        # query: tensor([[2054, 8681, 1037, 12042, 8738, 1029, 1064, 2003, 2009, 1996,
        #                 2168, 2005, 2273, 2004, 2092, 2004, 2308, 1029, 1064],
        #                [2054, 2001, 1996, 19061, 4329, 1029, 1064, 0, 0, 0,
        #                 0, 0, 0, 0, 0, 0, 0, 0, 0]],
        #               device='cuda:0')
        # document: tensor([[2053, 1012, 2070, 2111, 2342, 1037, 2235, 12042, 8738, 2084,
        #                    2500, 2138, 2027, 2123, 1005, 1056, 2031, 2037, 5699, 2008,
        #                    2152, 1010, 2021, 2500, 2031, 1037, 3469, 8738, 2138, 1997,
        #                    2037, 2152, 5699, 1012, 1996, 2152, 10514, 1037, 29649, 11721,
        #                    5596, 5381, 2342, 1037, 12042, 8738, 2021, 2500, 2123, 1005,
        #                    1056, 1012, 2061, 2035, 2111, 2123, 1005, 1056, 2342, 1996,
        #                    2168, 12042, 8738, 1012, 18168, 2063, 2111, 2342, 1037, 2235,
        #                    12042, 8738, 2084, 2500, 2138, 2027, 2123, 1005, 1056, 2031,
        #                    2037, 5699, 2008, 2152, 1010, 2021, 2500, 2031, 1037, 3469,
        #                    8738, 2138, 1997, 2037, 2152, 5699, 1012, 1996, 2152, 10514,
        #                    1037, 29649, 11721, 5596, 5381, 2342, 1037, 12042, 8738, 2021,
        #                    2500, 2123, 1005, 1056, 1012, 2061, 2035, 2111, 2123, 1005,
        #                    1056, 2342, 1996, 2168, 12042, 8738, 1012],
        #                   [19061, 4329, 1024, 2339, 2001, 2009, 2590, 1029, 1996, 19061,
        #                    4329, 2001, 2590, 2138, 2012, 2023, 2051, 2111, 2318, 7876,
        #                    1010, 2047, 6786, 2020, 2108, 8826, 1998, 2111, 2318, 2746,
        #                    2362, 1012, 1996, 19061, 4329, 2211, 2043, 2111, 2318, 2000,
        #                    3218, 7876, 1012, 2070, 2903, 7876, 2318, 1999, 1996, 2690,
        #                    2264, 1010, 1998, 2500, 3373, 7876, 2764, 9174, 1999, 2367,
        #                    4655, 1012, 2833, 1011, 7215, 2308, 3603, 2008, 2065, 8079,
        #                    2020, 8461, 2006, 1996, 2598, 1010, 2047, 4264, 2052, 4982,
        #                    1999, 1037, 2095, 1012, 0, 0, 0, 0, 0, 0,
        #                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        #                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        #                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        #                    0, 0, 0, 0, 0, 0, 0]], device='cuda:0')
        # initial_score: tensor([[0.9585],
        #                        [2.9595]], device='cuda:0')
        # q_turn: tensor([[0., 0., 1., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
        #         [0., 1., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.]], device='cuda:0')

        if self.combine_normalised_scores:
            initial_score = initial_score.view(initial_score.shape[0], -1)

        if self.mask_q_turn is not None:
            q_turn = q_turn[:, :self.mask_q_turn]
            q_turn[q_turn.sum(dim=1) == 0, self.mask_q_turn-1] = 1.0
            assert (q_turn .sum(dim=1) == 1).all()

        out = self.bert_ranker.forward(query_tok=query, query_mask=None, doc_tok=document, doc_mask=None)
        # out.shape: torch.Size([2, 1])
        out = self.tanh_bert(out)
        out = torch.cat((out, initial_score, q_turn), dim=1)
        # out.shape: torch.Size([2, 5])
        out = self.linear(out)
        # out.shape: torch.Size([2, 1])

        if self.apply_tanh_bert_output:
            out = self.tanh(out)
            # out.shape: torch.Size([2, 1])

        # initial out:
        # out.shape: torch.Size([2, 1])
        # out: tensor([[-0.0305],
        #              [0.0130]], device='cuda:0', grad_fn= < TanhBackward >)
        return out


class BERTModelGlobalPlus(BERTModelGlobalFeatures):
    """
    A variation of the global features model for experimentation
    """
    def __init__(self, device, ranker_name='vanilla', dropout_prob=0.1, apply_tanh_bert_output=False, eps=1e-10, mask_q_turn=None):
        super(BERTModelGlobalPlus, self).__init__(device, global_do_layer_norm = None, ranker_name=ranker_name,
                                  dropout_prob=dropout_prob, apply_tanh_bert_output=apply_tanh_bert_output, eps=eps)

        self.mask_q_turn = mask_q_turn
        self.tanh_bert = torch.nn.Tanh()

        # TODO change 15 to parameter #num questions + initial score + quer-doc score
        n_input_linear = 15 if self.mask_q_turn is None else self.mask_q_turn + 2

        # Reparametrization
        self.mean = nn.Linear(n_input_linear, n_input_linear*2)
        self.std = nn.Linear(n_input_linear, n_input_linear*2)
        self.linear_rep = nn.Linear(n_input_linear*2, 1)
        self.dropout_rep = nn.Dropout(0.33)

    def predict(self, query, document, initial_score, q_turn):
        # query.shape: torch.Size([2, 19])
        # document.shape: torch.Size([2, 86])
        # initial_score.shape: torch.Size([2, 1])
        # q_turn.shape: torch.Size([2, 13])

        if self.mask_q_turn is not None:
            q_turn = q_turn[:, :self.mask_q_turn]
            q_turn[q_turn.sum(dim=1) == 0, self.mask_q_turn-1] = 1.0
            assert (q_turn .sum(dim=1) == 1).all()

        out = self.bert_ranker.forward(query_tok=query, query_mask=None, doc_tok=document, doc_mask=None)
        # out.shape: torch.Size([2, 1])
        out = self.tanh_bert(out)
        out = torch.cat((out, initial_score, q_turn), dim=1)
        # out.shape: torch.Size([2, 5])

        ###
        # Reparametrization
        # if self.training:
        mean = self.mean(out)
        std = F.softmax(self.std(out))
        out = torch.randn_like(mean) * std + mean
        out = F.relu(self.dropout_rep(out))
        out = self.linear_rep(out)
        # else:
        #     out = self.mean(out)
        ###

        if self.apply_tanh_bert_output:
            out = self.tanh(out)
            # out.shape: torch.Size([2, 1])

        # initial out:
        # out.shape: torch.Size([2, 1])
        # out: tensor([[-0.0305],
        #              [0.0130]], device='cuda:0', grad_fn= < TanhBackward >)
        return out


class BERTModelGlobalPlus2(BERTModelGlobalFeatures):
    """
    A variation of the global features model for experimentation
    """
    def __init__(self, device, ranker_name='vanilla', dropout_prob=0.1, apply_tanh_bert_output=False, eps=1e-10, mask_q_turn=None):
        super(BERTModelGlobalPlus2, self).__init__(device, global_do_layer_norm = None, ranker_name=ranker_name,
                                  dropout_prob=dropout_prob, apply_tanh_bert_output=apply_tanh_bert_output, eps=eps)

        self.mask_q_turn = mask_q_turn
        self.tanh_bert = torch.nn.Tanh()
        self.relu = torch.nn.ReLU()

        # TODO change 15 to parameter #num questions + initial score + quer-doc score
        n_input_linear = 15 if self.mask_q_turn is None else self.mask_q_turn + 2

        self.linear_1 = nn.Linear(n_input_linear, n_input_linear * 2)
        self.linear_2 = nn.Linear(n_input_linear * 2, 1)

    def predict(self, query, document, initial_score, q_turn):
        # query.shape: torch.Size([2, 19])
        # document.shape: torch.Size([2, 86])
        # initial_score.shape: torch.Size([2, 1])
        # q_turn.shape: torch.Size([2, 13])
        if self.mask_q_turn is not None:
            q_turn = q_turn[:, :self.mask_q_turn]
            q_turn[q_turn.sum(dim=1) == 0, self.mask_q_turn-1] = 1.0
            assert (q_turn .sum(dim=1) == 1).all()

        out = self.bert_ranker.forward(query_tok=query, query_mask=None, doc_tok=document, doc_mask=None)
        out = self.tanh_bert(out)
        out = torch.cat((out, initial_score, q_turn), dim=1)
        out = self.linear_2(self.relu(self.linear_1(out)))
        if self.apply_tanh_bert_output:
            out = self.tanh(out)

        return out


class BERTModelGlobalPlus3(BERTModelGlobalFeatures):
    """
    A variation of the global features model for experimentation
    """
    def __init__(self, device, ranker_name='vanilla', dropout_prob=0.1, apply_tanh_bert_output=False, eps=1e-10, mask_q_turn=None):
        super(BERTModelGlobalPlus3, self).__init__(device, global_do_layer_norm = None, ranker_name=ranker_name,
                                  dropout_prob=dropout_prob, apply_tanh_bert_output=apply_tanh_bert_output, eps=eps)

        self.mask_q_turn = mask_q_turn
        self.tanh_bert = torch.nn.Tanh()
        self.relu = torch.nn.ReLU()

        # TODO change 15 to parameter #num questions + initial score + quer-doc score
        n_input_linear = 15 if self.mask_q_turn is None else self.mask_q_turn + 2

        self.linear_1 = nn.Linear(n_input_linear, n_input_linear * 2)
        self.linear_2 = nn.Linear(n_input_linear * 2, 1)
        self.dropout_hidden = nn.Dropout(0.3)

    def predict(self, query, document, initial_score, q_turn):
        # query.shape: torch.Size([2, 19])
        # document.shape: torch.Size([2, 86])
        # initial_score.shape: torch.Size([2, 1])
        # q_turn.shape: torch.Size([2, 13])
        if self.mask_q_turn is not None:
            q_turn = q_turn[:, :self.mask_q_turn]
            q_turn[q_turn.sum(dim=1) == 0, self.mask_q_turn-1] = 1.0
            assert (q_turn .sum(dim=1) == 1).all()

        out = self.bert_ranker.forward(query_tok=query, query_mask=None, doc_tok=document, doc_mask=None)
        out = self.tanh_bert(out)
        out = torch.cat((out, initial_score, q_turn), dim=1)
        out = self.linear_2(self.dropout_hidden(self.relu(self.linear_1(out))))
        if self.apply_tanh_bert_output:
            out = self.tanh(out)

        return out


class BERTModelGlobalPlus4(BERTModelGlobalFeatures):
    """
    A variation of the global features model for experimentation
    """
    def __init__(self, device, ranker_name='vanilla', dropout_prob=0.1, apply_tanh_bert_output=False, eps=1e-10,
                 mask_q_turn=None):
        super(BERTModelGlobalPlus4, self).__init__(device, global_do_layer_norm=None, ranker_name=ranker_name,
                                                   dropout_prob=dropout_prob,
                                                   apply_tanh_bert_output=apply_tanh_bert_output, eps=eps)

        self.mask_q_turn = mask_q_turn
        self.tanh_bert = torch.nn.Tanh()
        n_input_linear = 15 if self.mask_q_turn is None else self.mask_q_turn + 2

        # Reparametrization
        self.mean = nn.Linear(n_input_linear, n_input_linear * 2)
        self.std = nn.Linear(n_input_linear, n_input_linear * 2)
        self.linear_rep = nn.Linear(n_input_linear * 2, 1)
        self.dropout_rep = nn.Dropout(0.33)
        self.std_mean = None

    def predict(self, query, document, initial_score, q_turn):
        # query.shape: torch.Size([2, 19])
        # document.shape: torch.Size([2, 86])
        # initial_score.shape: torch.Size([2, 1])
        # q_turn.shape: torch.Size([2, 13])

        if self.mask_q_turn is not None:
            q_turn = q_turn[:, :self.mask_q_turn]
            q_turn[q_turn.sum(dim=1) == 0, self.mask_q_turn - 1] = 1.0
            assert (q_turn.sum(dim=1) == 1).all()

        out = self.bert_ranker.forward(query_tok=query, query_mask=None, doc_tok=document, doc_mask=None)
        # out.shape: torch.Size([2, 1])
        out = self.tanh_bert(out)
        out = torch.cat((out, initial_score, q_turn), dim=1)
        # out.shape: torch.Size([2, 5])

        ###
        # Reparametrization
        mean = self.mean(out)
        std = F.softmax(self.std(out))
        self.std_mean = torch.mean(std, dim=1)
        out = torch.randn_like(mean) * std + mean
        out = F.relu(self.dropout_rep(out))
        out = self.linear_rep(out)


        if self.apply_tanh_bert_output:
            out = self.tanh(out)
            # out.shape: torch.Size([2, 1])

        return out

    def train_pairwise(self, data):
        # print("data.keys(): {}".format(data.keys()))
        # print("data['q_turn']: {}".format(data['q_turn'])) #TODO remove
        # assert False
        q_turn = data['q_turn']

        query_texts = data['q_text']

        pos_texts = data['doc_text']
        pos_initial_scores = data['initial_score_pos']

        neg_texts = data['neg_doc_text']
        neg_initial_scores = data['initial_score_neg']

        pos_scores = self.predict(query_texts, pos_texts, pos_initial_scores, q_turn)
        neg_scores = self.predict(query_texts, neg_texts, neg_initial_scores, q_turn)

        # TODO make sure this loss is correct
        labels = torch.ones(pos_scores.size(), requires_grad=False).to(self.device)

        loss = self.margin_ranking_loss(pos_scores, neg_scores, labels) - 2*torch.log(self.std_mean) # self.std_mean in [0, 1]
        return loss
