## ILPS TREC CAsT 2019 challenge submission

# Knowledge transfer with global conversational embeddingds got dialogue ranking using BERT

To get the best performing results as reported run:
```
sbatch train_andreas.sh pretrain_param_default_andreas.txt
sbatch train_andreas.sh train_param_global_QUAC.txt
sbatch train_andreas.sh train_param_global_best_resume_from_QUAC_drop_plus.txt
```

This projects follows the experimentation of:
1. Performing unsupervised language ranking of top 2000 documents per query
2. Pre-training reranking on the MS MARCO ranking triples dataset
3. Pre-training reranking on the QuAC dataset
4. Fine-tuning on the TREC CAsT 2019 training topics

The files (a) 'train_param_*' , 'pretrain_param_*'  and 'test_param_*' contain the parameter configuration for a single experimentation procedure.
The src code is located in 'cast/src' with the train and test script at 'cast/src/train.py' and 'cast/src/test.py' respectively.

For questions please email:
Andreas Panteli
```
andreas <dot> panteli <at> student <dot> uva <dot> nl
```
