import os
import numpy as np

from torch.utils.data import DistributedSampler, RandomSampler

from pytorch_pretrained_bert import BertAdam

from dataset.msmarco_pretraining_dataset import PretrainingTriplesDataset, MARCODevDataset
from dataset.ranking_dataset import RankingDataset
from models.models import RepresentationModel, BERTModel, BERTModelGlobalFeatures, BERTModelGlobalPlus, \
    BERTModelGlobalPlus2, BERTModelGlobalPlus3, BERTModelGlobalPlus4
import torch
import time
from utils import dict_to_cuda
import copy


def train_epoch(epoch, dataloader, model, optimizer, train_losses, config, using_cuda, device='cpu'):
    """
    Performs the training during one epoch
    :param epoch: the current epoch number
    :param dataloader: the loader containing the iteration object for the training data
    :param model: the model to use
    :param optimizer: the optimiser to use for training
    :param train_losses: the train losses list
    :param config: the configuration argument set
    :param using_cuda: boolean variable indicating whether to use the GPU
    :param device: the device to use ("cuda" or 'cpu')
    """
    model.train()
    s_time = time.time()
    losses = []
    for batch_id, batch in enumerate(dataloader, 0):
        optimizer.zero_grad()
        if using_cuda:
            batch = dict_to_cuda(batch)

        loss = model(batch, True)
        # TODO refactor
        loss = loss.mean() if not isinstance(loss, tuple) else torch.cat(loss, dim=-1).mean()

        # For the partially disabled learning part. Saves the initial weight values
        if not config.drop_plus_prob is None:
            state2 = copy.copy(model.state_dict())

        loss.backward()
        if config.clip_grad_norm > 0:
            torch.nn.utils.clip_grad_norm_(model.parameters(), config.clip_grad_norm)
        optimizer.step()
        loss_val = loss.detach().cpu().numpy()

        # Perform partially disabled learning. Samples a number of weights from the model after the backpropagation
        #   and resets their value to the one previously saved before the update
        if not config.drop_plus_prob is None:
            totally_the_same = True
            for param_tensor in state2:
                mask_weights = torch.FloatTensor(state2[param_tensor].size()).uniform_() > config.drop_plus_prob
                model.state_dict()[param_tensor] = model.state_dict()[param_tensor].type(torch.FloatTensor).to(
                    device) * mask_weights.type(torch.FloatTensor).to(device) + (
                                                           1 - mask_weights.type(torch.FloatTensor).to(
                                                       device)) * state2[param_tensor].type(
                    torch.FloatTensor).to(device)
                if not (state2[param_tensor].cpu().numpy() == model.state_dict()[
                    param_tensor].cpu().numpy()).all():
                    totally_the_same = False
            if totally_the_same:
                assert Exception('Totally the same')

        if epoch <= 1 and (batch_id + 1) % 10 == 0:
            print('{}/{} batches in {:.2f}m. Loss: {}'.format(batch_id + 1, len(dataloader),(time.time() - s_time) / 60, loss_val))
        losses.append(loss_val)

    mean_loss = np.mean(losses)
    train_losses.append(mean_loss)


def initialize_dataset(tokenizer, using_cuda, qrels_file, run_file, query_text_file, anserini_index_path, mode,
                       num_neg=None, do_normalize_initial_score=True, do_cap_to_top_docs=-1,
                       combine_normalised_scores=False, train_global_QUAC=False):
    """
    Initialises the dataset for the TREC CAsT data
    :param tokenizer: the pre-trained tokeniser
    :param using_cuda: whether using the GPU
    :param qrels_file: the qrels file to load for the data
    :param run_file: the ranked list run file from the unsupervised language model to load
    :param query_text_file: the file containing the dialogues
    :param anserini_index_path: the path to the anserini index of the passages' collection
    :param mode: the mode, whether it is training ("train") or evaluating ("eval")
    :param num_neg: the number of negative samples to produce per query
    :param do_normalize_initial_score: indicating if the initial score from the unsupervised model should be normalised
    :param do_cap_to_top_docs: number of documents to cap at. with -1 no capping is done
    :param combine_normalised_scores: indicating to combine the initial scores, unormalised, with their normalised versions
    :param train_global_QUAC: whether to use the global features for the QuAC dataset
    :return: the dataset
    """
    loaded_dataset = RankingDataset(qrels_file, run_file, query_text_file, anserini_index_path, tokenizer, mode=mode,
                                    num_neg=num_neg, using_cuda=using_cuda,
                                    do_normalize_initial_score=do_normalize_initial_score,
                                    do_cap_to_top_docs=do_cap_to_top_docs,
                                    combine_normalised_scores=combine_normalised_scores,
                                    train_global_QUAC=train_global_QUAC)

    return loaded_dataset


def initialize_dataset_pretraining(tokenizer, using_cuda, is_training, run_file, max_num_docs, max_num_queries=None,
                                   eval_qrels=None):
    """
    Initialises the data when pre-training the model either on the MS MARCO dataset or the QuAC
    :param tokenizer: the pre-trained tokeniser
    :param using_cuda: whether to use the GPU
    :param is_training: whether it is training or evaluating
    :param run_file: the ranked list run file to load
    :param max_num_docs: the maximum number of documents to use for triples pairs
    :param max_num_queries: the maximum number of queries to use for the dataset
    :param eval_qrels: the qrels of the evaluation set
    :return: the dataset
    """
    if is_training:
        loaded_dataset = PretrainingTriplesDataset(run_file, tokenizer, max_num_docs=max_num_docs, using_cuda=using_cuda)
    else:
        loaded_dataset = MARCODevDataset(run_file, eval_qrels, tokenizer, max_num_docs=max_num_docs,
                                         max_num_queries=max_num_queries, using_cuda=using_cuda)

    return loaded_dataset


def initialize_dataloader(dataset, is_training, using_cuda, batch_size, num_workers, distributed_launch=False):
    """
    Initialises the dataloader for training
    :param dataset: the dataset object containing the iterable construction of the data to load every epoch
    :param is_training: whether it is training or evaluating
    :param using_cuda: whether using the GPU
    :param batch_size: the batch size
    :param num_workers: the number of workers for laoding the dataset
    :param distributed_launch: whether working with distributed loaders
    :return: the dataloader
    """
    if using_cuda and is_training:
        if distributed_launch:
            sampler = DistributedSampler(dataset)
        else:
            sampler = RandomSampler(dataset)
        loader = torch.utils.data.DataLoader(dataset=dataset, collate_fn=dataset.collate_fn, batch_size=batch_size,
                                             sampler=sampler, num_workers=num_workers)  # , pin_memory=True)
    else:
        shuffle = True if is_training else False
        loader = torch.utils.data.DataLoader(dataset=dataset, collate_fn=dataset.collate_fn, batch_size=batch_size,
                                             shuffle=shuffle, num_workers=num_workers)  # , pin_memory=True)
    return loader


def initialize_model(config, vocab2id, device):
    """
    Initialises the model to use
    :param config: the configuration argument set
    :param vocab2id: the vocabulary of the tokeniser
    :param device: the device using ("cuda" or "cpu")
    :return: model and the optimiser for training
    """
    # Representation: simple model for sanity check
    if config.model == 'representation':
        model = RepresentationModel(vocab_size=len(vocab2id), embedding_size=config.embedding_size, device=device,
                                    drop_prob=config.embedding_drop_prob)
    # Vanilla CEDR-KNRM BERT model
    elif config.model == 'bert':
        model = BERTModel(device=device, ranker_name=config.bert_ranker_name, dropout_prob=config.bert_dropout_prob,
                          apply_tanh_bert_output=config.apply_tanh_bert_output)
    # BERT model with global features
    elif config.model.lower() == "bert_global":
        model = BERTModelGlobalFeatures(device=device, global_do_layer_norm=config.global_do_layer_norm,
                                        ranker_name=config.bert_ranker_name, dropout_prob=config.bert_dropout_prob,
                                        apply_tanh_bert_output=config.apply_tanh_bert_output,
                                        mask_q_turn=config.global_mask_q_turn,
                                        combine_normalised_scores=config.combine_normalised_scores)
    # Variations of the BERT_global model for experimentation
    elif config.model.lower() == "global_plus":
        model = BERTModelGlobalPlus(device=device, ranker_name=config.bert_ranker_name,
                                    dropout_prob=config.bert_dropout_prob,
                                    apply_tanh_bert_output=config.apply_tanh_bert_output,
                                    mask_q_turn=config.global_mask_q_turn)
    elif config.model.lower() == "global_plus_2":
        model = BERTModelGlobalPlus2(device=device, ranker_name=config.bert_ranker_name,
                                     dropout_prob=config.bert_dropout_prob,
                                     apply_tanh_bert_output=config.apply_tanh_bert_output,
                                     mask_q_turn=config.global_mask_q_turn)
    elif config.model.lower() == "global_plus_3":
        model = BERTModelGlobalPlus3(device=device, ranker_name=config.bert_ranker_name,
                                     dropout_prob=config.bert_dropout_prob,
                                     apply_tanh_bert_output=config.apply_tanh_bert_output,
                                     mask_q_turn=config.global_mask_q_turn)
    elif config.model.lower() == "global_plus_4":
        model = BERTModelGlobalPlus4(device=device, ranker_name=config.bert_ranker_name,
                                     dropout_prob=config.bert_dropout_prob,
                                     apply_tanh_bert_output=config.apply_tanh_bert_output,
                                     mask_q_turn=config.global_mask_q_turn)
    else:
        raise NotImplementedError

    model = model.to(device)
    model.init_params(escape='bert', verbosity=config.verbosity)

    # Change the optimiser if using different BERT learning rate
    if config.use_different_BERT_lr:
        params = [(k, v) for k, v in model.named_parameters() if v.requires_grad]
        non_bert_params = {'params': [v for k, v in params if not 'bert' in k]}
        bert_params = {'params': [v for k, v in params if 'bert' in k], 'lr': config.Bert_lr}
        if config.use_bert_Adam:
            optimizer = BertAdam([non_bert_params, bert_params], lr=config.lr, warmup=0, t_total=1000 * 600)
        else:
            optimizer = torch.optim.Adam([non_bert_params, bert_params], lr=config.lr)

    else:
        optimizer = BertAdam(model.parameters(), lr=config.lr, warmup=0, t_total=1000 * 600)
    return model, optimizer


def start_evaluation_separately(config, save_model_name, evaluations_dir, epoch):
    """
    To prepare an evaluation command to the bash file running the evaluation job in order to separate them and not wait
    during training
    :param config: the configuration argument set
    :param save_model_name: the name of the saved model to use
    :param evaluations_dir: the directory to save the evaluation output
    :param epoch: the current epoch
    :return:
    """
    evaluations_log_filename = "Epoch_{}_log.txt".format(epoch)
    do_pretraining = ""
    if config.pretrain_marco:
        do_pretraining = "--pretrain_marco"

    do_cuda = ""
    if config.eval_use_cuda:
        do_cuda = "--cuda"

    do_normalize_initial_score = ""
    if config.do_normalize_initial_score:
        do_normalize_initial_score = "--do_normalize_initial_score"

    combine_normalised_scores = ""
    if config.combine_normalised_scores:
        combine_normalised_scores = "--combine_normalised_scores"

    is_QUAC = ""
    if config.is_eval_QUAC:
        is_QUAC = '--is_QUAC'

    # Evaluating model
    cmd = 'sbatch {} "" {} --model_file {} --output_dir {} --output_run_filename Epoch_{}_run.trec --run_file {} ' \
          '--eval_query_text_file {} --anserini_index_path {} --identifier {} --max_docs {} ' \
          '--qrels {} {} --batch_size {} --output_log_filename {} --max_num_queries {} {} {}'\
        .format(config.testfile, do_cuda, save_model_name, evaluations_dir, epoch, config.eval_run_file,
                config.eval_query_text_file, config.anserini_index_path, config.identifier, config.max_eval_num_docs,
                config.eval_qrels_file, do_pretraining, config.eval_batch_size, evaluations_log_filename,
                config.max_eval_num_queries, do_normalize_initial_score, combine_normalised_scores, is_QUAC)
    print(cmd)
    return cmd


def send_command_if_file_EOF(check_file, cmds, evaluate_epochs_list, queued_epoch=0):
    """
    Check if previous evaluation has been finished and submit a new job to the cluster
    :param check_file: the previous evaluation file to check if it has finished
    :param cmds: the commands list
    :param evaluate_epochs_list: the epoch list for the evaluations still pending
    :param queued_epoch: the current epoch queued to be evaluated
    :return: the updated command list, and epochs if submitted a new job
    """
    if os.path.exists(check_file) and len(open(check_file, 'r').readlines()) > 0 and \
            open(check_file, 'r').readlines()[-1] == "EOF":
        os.system(cmds[0])
        print("Submitted: {}".format(cmds[0]))
        evaluate_epochs_list.pop(queued_epoch)
        cmds.pop(0)
        queued_epoch -= 1 if queued_epoch > 0 else 0

    return cmds, evaluate_epochs_list, queued_epoch
