#!/bin/sh

echo "Are you sure you want to delete ALL files in the results directory?"

echo "If yes, write:"
echo "I am not stupid and I acknowledge this will remove ALL results saved, IRRIVERSIBLY"
RE= read -p "Here: " RE

str="I am not stupid and I acknowledge this will remove ALL results saved, IRRIVERSIBLY"

str=$(echo "$str" | tr '[:upper:]' '[:lower:]')
str=$(echo "$str" | tr -d ' ')
RE=$(echo "$RE" | tr '[:upper:]' '[:lower:]')
RE=$(echo "$RE" | tr -d ' ')

if [ "$RE" == "$str" ] 
then
    if [ ! -d ".backup" ]
    then
       mkdir ".backup"
    fi
    if [ -d "cast/results/" ]
    then
        mv cast/results/ .backup/ -f
        echo "Everything is removed"
    else
        echo "There was nothing to remove :("
    fi
else
    echo "I thought so."
fi
