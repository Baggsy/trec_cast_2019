import torch
import torch.nn as nn
from torch.nn.init import xavier_uniform_


class AbstractModel(nn.Module):
    """
    Abstract model that implements the basic prediction and scoring depending whether it is evaluating the loss function
    for the training data or the metrics' score for the evaluation set
    """
    def __init__(self, device, eps=1e-10, margin=1.0):
        super(AbstractModel, self).__init__()
        self.device = device
        self.eps = eps
        self.margin_ranking_loss = nn.MarginRankingLoss(margin=margin, reduction='mean').to(self.device)

    def predict(self, query, document, initial_score, q_turn=None):
        raise NotImplementedError

    def train_pairwise(self, data):
        q_turn = data['q_turn']
        query_texts = data['q_text']

        pos_texts = data['doc_text']
        pos_initial_scores = data['initial_score_pos']

        neg_texts = data['neg_doc_text']
        neg_initial_scores = data['initial_score_neg']

        pos_scores = self.predict(query_texts, pos_texts, pos_initial_scores, q_turn)
        neg_scores = self.predict(query_texts, neg_texts, neg_initial_scores, q_turn)

        labels = torch.ones(pos_scores.size(), requires_grad=False).to(self.device)

        loss = self.margin_ranking_loss(pos_scores, neg_scores, labels)
        return loss

    def forward(self, data, is_training):
        if is_training:
            return self.train_pairwise(data)
        else:
            return self.predict(data['q_text'], data['doc_text'], data['initial_score'], data['q_turn'])

    def init_params(self, escape=None, verbosity=1):
        for name, param in self.named_parameters():
            if escape is not None and escape in name:
                if verbosity >= 2:
                    print('no_init', name, param.size())
                continue
            if verbosity >= 2:
                print('init', name, param.size())
            if param.data.dim() > 1:
                xavier_uniform_(param.data)