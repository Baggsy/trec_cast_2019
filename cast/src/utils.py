import numpy as np
import random
import time

from torch.backends import cudnn
from torch.distributions.categorical import *
import os
import datetime
import logging
import re
import sys
import pytrec_eval
from collections import defaultdict
import torch.distributed as dist


def log_epoch_info(log, t, epoch, epochs, train_loss, eval_loss, optimisation_metric, opt_metric_score):
    """
    Prints the logging information for the just finished epoch
    :param log: logger object
    :param t: the time in minutes the finished epoch required
    :param epoch: the finished epoch
    :param epochs: the total number of epochs
    :param train_loss: the training loss for this epoch
    :param eval_loss: the evaluation loss for this epoch
    :param optimisation_metric: the optimisation metric name
    :param opt_metric_score: the optimisation metric value
    :return: print epoch information
    """
    log.info("[{:.2f}m] - {:3}/{:3} - Train Loss: {:.4f} Eval Loss: {:.4f} \t Eval {} {:.4f}"
             .format(t, epoch, epochs, train_loss,
                     eval_loss, optimisation_metric,
                     opt_metric_score))


def eval_metrics(d_gold, d_preds, metrics):
    """
    Calculates the metric scores given labels and predictions
    :param d_gold: labels. keys: string, val: integer
    :param d_preds: predictions. keys: string, val: float
    :param metrics: are the metric names to be evaluated
    :return: the mean metric scores
    """
    evaluator = pytrec_eval.RelevanceEvaluator(d_gold, metrics)

    num_queries = len(d_gold)
    eval_dict_per_query = evaluator.evaluate(d_preds)

    return mean_queries(eval_dict_per_query, num_queries), eval_dict_per_query


def mean_queries(eval_dict, num_queries):
    """
    Calculates the average value of the dictionary with the metric scores
    :param eval_dict: dictionary containing the score per prediction per metric
    :param num_queries: number of predictions
    :return: a dictionary containing the average metric scores
    """
    mean_metrics = defaultdict(float)
    for k, v in eval_dict.items():
        for metric, val in v.items():
            mean_metrics[metric] += val

    return dict([(metric, val/num_queries) for metric, val in mean_metrics.items()])


def dict_to_cuda(batch):
    """
    Transfers the tensors of a dictionary to the gpu
    :param batch: the dictionary containing the batch items
    :return: the GPU-stored-tensors dictionary
    """
    batch_cuda = dict()
    for key, value in batch.items():
        if isinstance(value, torch.Tensor):
            batch_cuda[key] = value.cuda()
        else:
            batch_cuda[key] = value
    return batch_cuda


def set_seed(seed):
    """
    Sets the random seed for reproducability
    :param seed: integer value of the seed
    """
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    if torch.cuda.is_available():
        torch.cuda.manual_seed_all(seed)
        cudnn.enabled = True
        cudnn.benchmark = True
        cudnn.deterministic = True


def init_seed_get_device(config):
    """
    Sets the seed and initialises the device to be used (GPU or CPU)
    :param config: the configuration argument set
    :return: the device name ('CUDA' or 'CPU') and the boolean variable if using the GPU
    """
    set_seed(config.seed)
    if torch.cuda.is_available():
        if config.verbosity >= 2:
            print(torch.__version__)
            print(torch.version.cuda)
            print(cudnn.version())
        device_name = 'cuda'
        if config.distributed_launch:
            torch.distributed.init_process_group(backend='NCCL', init_method='env://') #world_size=config.world_size, rank=config.local_rank
            torch.cuda.set_device(config.local_rank)
            device_name += ":" + str(torch.cuda.current_device())
        using_cuda = True
    else:
        device_name = 'cpu'
        using_cuda = False

    return device_name, using_cuda


def get_exp_name(config):
    """
    Creates the experiment specific name and directories to save the model weights per epoch
    :param config: the configuration argument set
    :return: the experiment name (string), the experiment directory path (string), the starting epoch (int) ,0 if not
                resuming from previous trained model, the logger to log the printing and status of the program, the
                train losses list and the model checkpoint if resuming from previously trained model
    """
    exp_name, exp_dir, log = None, None, None
    if config.resume_from is not None:
        checkpoint = torch.load(config.resume_from)
        starting_epoch = checkpoint['epoch'] + 1
        train_losses = checkpoint['train_losses']
        # Only setup paths for the 0th local rank in case of distributed launch
        if config.local_rank == 0:
            model_dir = os.path.join(config.save_models_dir, config.model.lower())
            os.makedirs(model_dir, exist_ok=True)
            if config.continue_training_from_exp is not None and len(config.continue_training_from_exp) > 0:
                exp_name = config.continue_training_from_exp
            else:
                exp_name = "{}_Exp_{}".format(datetime.date.today(), 1 if len(os.listdir(model_dir)) == 0 else int(
                    sorted([a.replace('.checkists', '') for a in os.listdir(model_dir)], key=natural_keys)[-1].split('_')[-1]) + 1)
                starting_epoch = 1
                train_losses = []
            exp_dir = os.path.join(model_dir, exp_name)
            checkpoints_dir = os.path.join(exp_dir, "checkpoints")
            os.makedirs(checkpoints_dir, exist_ok=True)
            eval_dir = os.path.join(exp_dir, "evaluations")
            os.makedirs(eval_dir, exist_ok=True)
            log = return_logger(os.path.join(exp_dir, config.log_file + ".log"))
            log.info("Resumed model {} from epoch {}".format(config.resume_from, starting_epoch))
            log.info("Running model {}".format(exp_dir))
    else:
        starting_epoch = 1
        train_losses = []
        checkpoint = None
        if config.local_rank == 0:
            model_dir = os.path.join(config.save_models_dir, config.model.lower())
            os.makedirs(model_dir, exist_ok=True)
            exp_name = "{}_Exp_{}".format(datetime.date.today(), 1 if len(os.listdir(model_dir)) == 0 else int(
                sorted(os.listdir(model_dir), key=natural_keys)[-1].split('_')[-1]) + 1)
            exp_dir = os.path.join(model_dir, exp_name)
            checkpoints_dir = os.path.join(exp_dir, "checkpoints")
            os.makedirs(checkpoints_dir, exist_ok=True)
            eval_dir = os.path.join(exp_dir, "evaluations")
            os.makedirs(eval_dir, exist_ok=True)
            log = return_logger(os.path.join(exp_dir, config.log_file + ".log"))
            log.info("Initialised: {}/{}".format(model_dir, exp_name))
    return exp_name, exp_dir, starting_epoch, log, train_losses, checkpoint


def natural_keys(text):
    """
    Returns the key list for float numbers in file names for more proper sorting
    """

    def atof(text):
        """
        Used for sorting numbers, in float format
        """
        try:
            retval = float(text)
        except ValueError:
            retval = text
        return retval

    return [atof(c) for c in re.split(r'[+-]?([0-9]+(?:[.][0-9]*)?|[.][0-9]+)', text)]


def save_model(model, optimiser, filename, epoch, train_losses, eval_losses, best_score, config, eval_runs):
    """
    Saves the weights of the current trained model alongside all related information for this epoch
    :param model: the model as is trained after the current epoch
    :param optimiser: the optimiser at the stage of the current trained epoch
    :param filename: the name of the file to save the model with
    :param epoch: the current epoch
    :param train_losses: the train losses list
    :param eval_losses: the evaluation losses list
    :param best_score: the best score achieved until this epoch
    :param config: the configuration argument set
    :param eval_runs: the list of the evaluation ranking runs
    """
    params = {
        'model': model,
        'state_dict': model.state_dict(),
        'optimizer': optimiser.state_dict(),
        'config': config,
        'epoch': epoch,
        'train_losses': train_losses,
        'eval_losses': eval_losses,
        'best_score': best_score,
        'eval_runs': eval_runs
    }
    torch.save(params, filename)
    torch.save(model, filename + ".model")


def save_model_only(model, optimiser, filename, epoch, train_losses, config):
    """
    Saves the model as is trained at the current epoch (without the information about the evaluation set)
    :param model: the model as is trained at the current epoch
    :param optimiser: the optimiser at the stage of the current epoch
    :param filename: the name of the file to save the model at
    :param epoch: the current epoch
    :param train_losses: the train losses list
    :param config: the configuration argument set
    """
    params = {
        'model': model,
        'state_dict': model.state_dict(),
        'optimizer': optimiser.state_dict(),
        'config': config,
        'epoch': epoch,
        'train_losses': train_losses
    }
    torch.save(params, filename)
    torch.save(model, filename+".model")


def return_logger(log_file):
    """
    Creates the logger
    """
    log = logging.getLogger(__name__)
    log.setLevel(logging.DEBUG)
    # if os.path.isfile(log_file):
    #     os.remove(log_file)
    fh = logging.FileHandler(log_file)
    fh.setLevel(logging.DEBUG)
    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.INFO)
    formatter = logging.Formatter(fmt='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    log.addHandler(fh)
    log.addHandler(ch)

    return log


def get_ms():
    """
    Returns the time in milliseconds
    """
    return time.time() * 1000


def flatten_list(l):
    """
    Flattens a list iterating over all sub-lists
    """
    return [item for sublist in l for item in sublist]
