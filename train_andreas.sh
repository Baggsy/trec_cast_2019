#!/bin/sh
#SBATCH -o ./log/train_%A_%a.out
#SBATCH -e ./log/train_%A_%a.err
#SBATCH -n1
#SBATCH -p gpu
#SBATCH --gres=gpu:1
#SBATCH -c3
#SBATCH --mem=20G
#SBATCH --time=48:00:00
#SBATCH --array=1-1%1

N_PROC_PER_NODE=1

# for anserini
export JAVA_HOME="/usr/java/jdk1.8.0_144"
export PATH=$JAVA_HOME/bin:$PATH

# echo run info
echo "SLURM_SUBMIT_DIR="$SLURM_SUBMIT_DIR
echo "SLURM_JOB_ID"=$SLURM_JOB_ID
echo "SLURM_JOB_NAME"=$SLURM_JOB_NAME

# Set-up the environment.
source ${HOME}/.bashrc
source activate venv

HPARAMS_FILE="$1"
echo 'Parameter file: '$HPARAMS_FILE''

# Start the experiment.
echo "Param.: "$(head -$SLURM_ARRAY_TASK_ID $HPARAMS_FILE | tail -1)

python -u cast/src/train.py $(head -$SLURM_ARRAY_TASK_ID $HPARAMS_FILE | tail -1) --no_distributed_launch
if [[ $? = 0 ]]; then
    echo ""
    echo " ---- Finished running train.py ----"
    echo ""
else
    echo ""
    echo " ---- !X! Train.py failed !X! ---- "
    echo ""
fi

