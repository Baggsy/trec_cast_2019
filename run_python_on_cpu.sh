#!/bin/sh
#SBATCH -o ./log/%A.out
#SBATCH -e ./log/%A.err
#SBATCH -n1
#SBATCH -c3
#SBATCH --mem=20G
#SBATCH --time=30:00:00


# for anserini
export JAVA_HOME="/usr/java/jdk1.8.0_144"
export PATH=$JAVA_HOME/bin:$PATH

# echo run info
echo "SLURM_SUBMIT_DIR="$SLURM_SUBMIT_DIR
echo "SLURM_JOB_ID"=$SLURM_JOB_ID
echo "SLURM_JOB_NAME"=$SLURM_JOB_NAME

# Set-up the environment.
source ${HOME}/.bashrc
source activate venv


python -u "${@}"

if [[ $? = 0 ]]; then
    echo ""
    echo " ---- Finished running train.py ----"
    echo ""
else
    echo ""
    echo " ---- !X! Train.py failed !X! ---- "
    echo ""
fi

