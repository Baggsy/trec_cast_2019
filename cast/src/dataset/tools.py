import gzip
import json
import sys
import os
from collections import defaultdict
import operator
from tqdm import tqdm

from utils import flatten_list

# Temporary cache file
cached_tmp_path = '/ivi/ilps/projects/Trec_CAST_2019/cached_data/'


def read_query_text_dict(path, tokenizer, merge_text=False, question_delim='|', is_QUAC=False):
    """
    Reads the dialogue text file with the following structure into a dictionary
    1_1	What is a physician's assistant?
    1_2	What is a physician's assistant?;;What are the educational requirements required to become one?
    :param tokenizer: the pre-trained tokeniser
    :param question_delim: the deliminator of the queries
    :param merge_text: whether to merge the text or not
    :param path: the path to the text file
    :param is_QUAC: whether the text file contains the QUAC dialogues
    :return: the dictionary with the queries
    """
    query_name = os.path.basename(os.path.normpath(path)).replace('.tsv', '')
    if merge_text:
        query_name += '_merge_text'
    cached_filepath = os.path.join(cached_tmp_path, query_name + '.json.gz')
    if os.path.exists(cached_filepath):
        return json.load(gzip.open(cached_filepath, 'rt'))

    d = {}
    with open(path, 'r') as fin:
        for line in fin:
            qid, txt = line.split('\t')
            qid = correct_qid(qid, is_QUAC)
            query_turns_txt = txt.split(';;')
            query_turns_txt = [tokenizer.convert_tokens_to_ids(tokenizer.tokenize(x + question_delim))
                               for x in query_turns_txt]
            if merge_text:
                query_turns_txt = flatten_list(query_turns_txt)
            d[qid] = query_turns_txt

    json.dump(d, gzip.open(cached_filepath, 'wt'))

    return d


def correct_qid(qid_str, is_QUAC):
    """
    Correcting the id of the query because QuAC and Trec CAsT start from different id number
    :param qid_str: the Id in string
    :param is_QUAC: whether it is the QUAC dataset
    :return: the corrected id in string format
    """
    if is_QUAC:
        # qid_str: quac_0_0 -> 0_0
        return str(int(qid_str.split('_')[1])) + "_" + str(int(qid_str.split('_')[2]))
    else:
        # qid_str: 1_1 -> 0_0
        return str(int(qid_str.split('_')[0]) - 1) + "_" + str(int(int(qid_str.split('_')[1]) - 1))


def un_correct_qid(qid, is_QUAC):
    """
    Change the id of the query back for indexing
    :param qid: the if of the query in string
    :param is_QUAC: whether it is the QUAC dataset
    :return: the un-corrected id in string format
    """
    if is_QUAC:
        # qid_str: quac_0_0 -> 0_0 -> quac_0_0
        return "quac_" + qid
    else:
        # qid_str: 1_1 -> 0_0 -> 1_1
        return str(int(qid.split('_')[0]) + 1) + "_" + str(int(int(qid.split('_')[1]) + 1))


def calc_doc_norm_score(x, x_min, x_max):
    """
    calculates the score norm per query
    """
    return (x - x_min) / (x_max - x_min)


def min_max_norm_run_dict(run_dict):
    """
    Normalises the dictionary containing the run scores with the min, max per query
    :param run_dict: the un-normalised dictionary
    :return: the normalised run dictionary
    """
    max_per_query = {qid: max(doc_scores.values()) for qid, doc_scores in run_dict.items()}
    min_per_query = {qid: min(doc_scores.values()) for qid, doc_scores in run_dict.items()}

    normalized_run_dict = {}
    for qid, doc_scores in run_dict.items():
        q_run_dict = {docid: calc_doc_norm_score(doc_score, min_per_query[qid], max_per_query[qid])
                        for docid, doc_score in doc_scores.items()}
        normalized_run_dict[qid] = q_run_dict

    return normalized_run_dict


def combine_norm_run_dict(run_dict):
    """
    Normalises the dictionary containing the run scores with the min, max per query but also combines the un-normalised
    score
    :param run_dict: the un-normalised dictionary
    :return: the enhanced normalised run dictionary
    """
    max_per_query = {qid: max(doc_scores.values()) for qid, doc_scores in run_dict.items()}
    min_per_query = {qid: min(doc_scores.values()) for qid, doc_scores in run_dict.items()}

    normalized_run_dict = {}
    for qid, doc_scores in run_dict.items():
        q_run_dict = {docid: [calc_doc_norm_score(doc_score, min_per_query[qid], max_per_query[qid]), doc_score]
                        for docid, doc_score in doc_scores.items()}
        normalized_run_dict[qid] = q_run_dict

    return normalized_run_dict


def read_run_dict(filepath, is_QUAC=False):
    """
    Reads the run file into a dictionary
    """
    result = defaultdict(dict)
    for line in open(filepath, 'r'):
        qid, _, docid, rank, score, _ = line.split()
        qid = correct_qid(qid, is_QUAC)
        result[qid][docid] = float(score)
    return dict(result)


def lucene_doc_extractor(index_path, anserini_jar_path):
    """
    Extracts a document using Lucene
    """
    import jnius_config
    if not os.path.exists(anserini_jar_path):
        sys.stderr.write('missing bin/anserini.jar')
        sys.exit(1)
    jnius_config.set_classpath(anserini_jar_path)
    from jnius import autoclass
    index_utils = autoclass('io.anserini.index.IndexUtils')(index_path)

    def wrapped(docid):
        lucene_doc_id = index_utils.convertDocidToLuceneDocid(docid)
        if lucene_doc_id == -1:
            return None  # not found
        return index_utils.getRawDocument(docid)  # raw doc since we will process it with BERT

    return wrapped


def _extract_doc_ids_from_run_dict(run_dict):
    """
    Returns the set of all ids in the run dictionary
    """
    all_dids = set()
    for dids in run_dict.values():
        all_dids.update(dids.keys())
    return all_dids


def read_docs_text_dict(anserini_index_path, anserini_run_name, run_dict, tokenizer, max_num_tokens=512,
                        anserini_jar_path='/ivi/ilps/projects/Trec_CAST_2019/anserini/target/anserini-0.6.0-SNAPSHOT-fatjar.jar'):
    """
    Reads the dialogue text into a dictionary
    :param anserini_index_path: the path to the anserini index of the collections
    :param anserini_run_name: the name to use for caching the dictionary
    :param run_dict: the run dictionary
    :param tokenizer: the pre-trained tokeniser
    :param max_num_tokens: the maximum number of tokens to tokenise
    :param anserini_jar_path: the path to the anserini jar file
    :return: the dialogues' text dictionary
    """
    os.makedirs(cached_tmp_path, exist_ok=True)
    cached_filepath = os.path.join(cached_tmp_path, 'cacheddocs__{}.json.gz'.format(anserini_run_name))
    if os.path.exists(cached_filepath):
        return json.load(gzip.open(cached_filepath, 'rt'))
    doc_extractor = lucene_doc_extractor(anserini_index_path, anserini_jar_path)

    doc_ids = _extract_doc_ids_from_run_dict(run_dict)

    docs_text = dict()
    docs_not_in_index = 0
    for doc_id in tqdm(doc_ids, desc='loading doc text dict'):
        doc_text = doc_extractor(doc_id)
        if doc_text is None:
            docs_not_in_index += 1
            raise ValueError('{} not in index'.format(doc_id))
        doc_text = tokenizer.convert_tokens_to_ids(tokenizer.tokenize(doc_text)[:max_num_tokens])
        docs_text[doc_id] = doc_text

    print('writing docs_text_dict to file: {}'.format(cached_filepath))
    json.dump(docs_text, gzip.open(cached_filepath, 'wt'))
    print('Done writing to file.')

    return docs_text


def sort_dict_by_val(x, reverse=True):
    """
    Sorts a dictionary by its value
    """
    return sorted(x.items(), key=operator.itemgetter(1), reverse=reverse)


def cap_to_top_docs(runs_dict, top_n):
    """
    Limits the number of documents to the top k.
    :param runs_dict: the run dictionary
    :param top_n: the top k documents to keep
    :return: the new run dictionary
    """
    new_run = dict()
    for qid, doc_scores in runs_dict.items():
        q_run_sorted = sort_dict_by_val(doc_scores)[:top_n]
        q_run_dict = {docid: score for docid, score in q_run_sorted}
        new_run[qid] = q_run_dict

    return new_run
