import argparse


def main():
    """
    Changes the indentifier name string from a run in Trec CAsT format
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--run_file", type=str, required=True)
    parser.add_argument("--new_identifier", type=str, required=True)
    parser.add_argument("--max_docs", type=int, default=1000)
    args = parser.parse_args()

    second_run_filename = args.run_file+"2.trec"
    print("Changing identifier of file {} to {} at {}".format(args.run_file, args.new_identifier, second_run_filename))
    run_file = open(args.run_file, "r")
    second_run_file = open(second_run_filename, "w")

    # CAST2019-TOPIC31-TURN01 Q0 marco-789620 1 2.6661999225616455 ilps-uva-lm1
    # CAST2019-TOPIC31-TURN01 Q0 marco-3090847 2 2.6491000652313232 ilps-uva-lm1
    lines = run_file.readlines()
    for line_id, line in enumerate(lines):
        turn_id, q_id, d_id, rank, score, _ = line.split(' ')
        if int(rank) > args.max_docs:
            continue
        newline = turn_id + " " + q_id + " " + d_id + " " + rank + " " + score + " " + args.new_identifier
        second_run_file.write(newline)
        second_run_file.write("\n")
    second_run_file.close()
    run_file.close()


if __name__ == '__main__':
    main()
