import argparse
import subprocess
import time
import os
import pickle
import torch

from train_helper_fns import initialize_dataset_pretraining, initialize_dataset, initialize_dataloader, \
    initialize_model, start_evaluation_separately, train_epoch, send_command_if_file_EOF
from utils import init_seed_get_device, save_model_only, get_exp_name
from pytorch_pretrained_bert import BertTokenizer
from early_stopping import EarlyStopping


def train(config):
    # Set random seed + Initialise device
    device_name, using_cuda = init_seed_get_device(config)
    device = torch.device(device_name)

    # Get experiment name and setup directory paths
    exp_name, exp_dir, starting_epoch, log, train_losses, checkpoint = get_exp_name(config)
    evaluations_dir = os.path.join(exp_dir, 'evaluations')

    # Log information for the experiment
    log.info(config)

    # Setup early stopping object to keep track of when to stop training
    if config.early_stopping:
        early_sopping = EarlyStopping(mode=config.early_stopping_mode, min_delta=config.early_stopping_delta,
                                      patience=config.early_stopping_patience, percentage=False)

    # Setup tokenizer for the text and the vocabulary
    tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
    vocab2id = tokenizer.vocab

    # Initialising dataset
    if config.pretrain_marco:
        train_dataset = initialize_dataset_pretraining(tokenizer, using_cuda, is_training=True,
                                                       run_file=config.train_run_file,
                                                       max_num_docs=config.max_train_num_docs, eval_qrels=None)
    else:
        train_dataset = initialize_dataset(tokenizer, using_cuda, config.train_qrels_file, config.train_run_file,
                                           config.train_query_text_file, config.anserini_index_path, mode='train',
                                           num_neg=config.num_neg,
                                           do_normalize_initial_score=config.do_normalize_initial_score,
                                           do_cap_to_top_docs=config.do_cap_to_top_docs,
                                           combine_normalised_scores=config.combine_normalised_scores,
                                           train_global_QUAC=config.train_global_QUAC)

    # Initialise dataloader
    train_dataloader = initialize_dataloader(train_dataset, True, using_cuda, config.batch_size,
                                             num_workers=config.num_workers,
                                             distributed_launch=config.distributed_launch)

    # Initialise model
    model, optimizer = initialize_model(config, vocab2id, device)
    if checkpoint is not None:
        model.load_state_dict(checkpoint['state_dict'], strict=config.scrict_weights_loading)
        if config.scrict_weights_loading:
            optimizer.load_state_dict(checkpoint['optimizer'])
        del checkpoint
    if config.distributed_launch:
        model = torch.nn.parallel.DistributedDataParallel(model, device_ids=[config.local_rank],
                                                          output_device=config.local_rank, find_unused_parameters=True)

    # Check if bert should be fixed
    if starting_epoch < config.start_train_bert_at_epoch:
        print("Fixing bert at epoch: {}".format(starting_epoch))
        model.fix_bert()
    else:
        print("Setting bert to train at epoch: {}".format(starting_epoch))
        model.train_bert()

    # Evaluate before start training
    if config.local_rank == 0 and starting_epoch == 1:
        save_model_name = os.path.join(exp_dir, 'checkpoints', "Epoch_0")
        save_model_only(model, optimizer, save_model_name, 0, train_losses, config)
        cmd = start_evaluation_separately(config, save_model_name, evaluations_dir, 0)
        os.system(cmd)

    # Initialise the list for evaluation commands which are submitted to the cluster separately
    #     The early stopping list is a separate one to disentangle the separate evaluation commands
    evaluate_epochs_list = list(range(starting_epoch-1, config.epochs + 1))
    cmds = []
    early_stopping_epochs_to_check = list(range(starting_epoch-1, config.epochs + 1))
    early_stopping_break = False

    # Start training
    for epoch in range(starting_epoch, config.epochs + 1):
        t1 = time.time()

        # Set BERT weights for training
        if epoch == config.start_train_bert_at_epoch:
            print("Setting bert to train at epoch: {}".format(epoch))
            model.train_bert()

        # Perform the training epoch
        train_epoch(epoch, train_dataloader, model, optimizer, train_losses, config, using_cuda, device=device)

        # Lof information and save model at epoch
        if config.local_rank == 0:
            log.info("[{:.2f}m] - {:3}/{:3} - Train Loss: {:.4f}".format((time.time() - t1) / 60, epoch, config.epochs,
                                                                         train_losses[-1]))

            save_model_name = os.path.join(exp_dir, 'checkpoints', "Epoch_{}".format(epoch))
            save_model_only(model, optimizer, save_model_name, epoch, train_losses, config)
            cmd = start_evaluation_separately(config, save_model_name, evaluations_dir, epoch)
            cmds.append(cmd)

        # Check early stopping condition
        if config.early_stopping:
            if len(early_stopping_epochs_to_check) > 0:
                epoch_to_check = early_stopping_epochs_to_check[0]
                score_log_file = os.path.join(evaluations_dir, "Epoch_{}_log.txt".format(epoch_to_check))

                # Check if evaluation jobs previously submitted are finished (which means that len(file) > 0)
                while os.path.exists(score_log_file) and len(open(score_log_file, 'r').readlines()) > 0 \
                        and open(score_log_file, 'r').readlines()[-1] == "EOF":
                    eval_score = float(open(score_log_file, 'r').readlines()[-2])
                    if early_sopping.step(eval_score):
                        early_stopping_break = True
                        break

                    early_stopping_epochs_to_check.pop(0)
                    if len(early_stopping_epochs_to_check) == 0:
                        break

                    epoch_to_check = early_stopping_epochs_to_check[0]
                    score_log_file = os.path.join(evaluations_dir, "Epoch_{}_log.txt".format(epoch_to_check))

            if early_stopping_break:
                print("\nEarly stopping criterion reached\n")
                break

        # Submit the job to be evaluated on the current epoch
        if len(cmds) > 0:
            assert len(evaluate_epochs_list) > 0
            check_file = os.path.join(evaluations_dir, "Epoch_{}_log.txt".format(evaluate_epochs_list[0]))
            cmds, evaluate_epochs_list, _ = send_command_if_file_EOF(check_file, cmds, evaluate_epochs_list)

    # Print the commands when finished
    print(cmds)
    print(evaluate_epochs_list)

    # Submit job to evaluate all commands, on all remaining epochs in sequence
    if len(cmds) > 0:
        assert len(evaluate_epochs_list) > 0
        check_list_filename = "{}.checkists".format(exp_dir).replace("/", "_")
        pickle.dump([cmds, evaluate_epochs_list], open(check_list_filename, "wb"))
        cmd = 'sbatch run_python_on_cpu.sh cast/src/run_tests_queue.py --check_list_filename {} --evaluations_dir {}'.format(
            check_list_filename, evaluations_dir)
        os.system(cmd)

    print("Maximum memory cached for the whole duration of the experiment: {}".format(torch.cuda.max_memory_cached()))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--local_rank", type=int, default=0,
                        help="the rank used for distributed launching of the code in order to parallelize the training")
    parser.add_argument('--world-size', type=int, default=1, help="the world size used for distributed launching")
    parser.add_argument("--batch_size", type=int, default=4, help="the batch size, how many questions to load at a time")
    parser.add_argument("--eval_batch_size", type=int, default=100,
                        help="the evaluation batch size which takes the ranked list items to score")
    parser.add_argument("--lr", type=float, default=3e-6, required=True, help="the learning rate for training")
    parser.add_argument("--clip_grad_norm", type=float, default=0,
                        help="clipping of the norm to avoid oversized weights")
    parser.add_argument("--epochs", type=int, default=30, help="the number of epochs")
    parser.add_argument("--seed", type=int, default=42, help="the random seed")
    parser.add_argument("--verbosity", type=int, default=1,
                        help="the level of how verbose the logging of the code to be")
    parser.add_argument("--log_file", type=str, default="output", help="the log file to output the printing information")
    parser.add_argument("--model", type=str, default="representation", help="the neural model to use")
    parser.add_argument("--save_models_dir", type=str, required=True, help="the directory to save the experiments")
    parser.add_argument("--embedding_size", type=int, default=512,
                        help="the embedding size for the representation model")
    parser.add_argument("--hidden_size", type=int, default=256,
                        help="the hidden layer size for the representation model")
    parser.add_argument("--num_workers", type=int, default=2, help='# workers for the data loaders')
    parser.add_argument("--optimisation_metric", type=str, default='recip_rank',
                        help="which metric to use a the decisive one")
    parser.add_argument("--metrics", type=str, default='map,ndcg,recip_rank', help="the metrics to score the evaluation")
    parser.add_argument("--no_early_stopping", dest="early_stopping", action="store_false",
                        help="whether to perform early stopping")
    parser.add_argument("--early_stopping_mode", type=str, default="max",
                        help="whether the optimisation metric needs to be minimised (min) or maximised(max)")
    parser.add_argument("--early_stopping_delta", type=float, default=0.001,
                        help="the difference to signal convergence")
    parser.add_argument("--early_stopping_patience", type=int, default=3,
                        help="the number of epochs to wait after convergence has been reached")
    parser.add_argument("--start_train_bert_at_epoch", type=int, default=0, required=True,
                        help="unfreeze the weights of the pre-trained BERT model epoch")

    parser.add_argument("--apply_tanh_bert_output", dest="apply_tanh_bert_output", action="store_true",
                        help="whether to apply a tanh to the output")

    parser.add_argument("--train_run_file", type=str, required=True,
                        help="the file containing the ranked list of the training set run")
    parser.add_argument("--train_qrels_file", type=str, help="the qrels file for the training set")
    parser.add_argument("--eval_run_file", type=str, required=True,
                        help="the file containing the ranked list of the evaluation set run")
    parser.add_argument("--eval_qrels_file", type=str, required=True, help="the qrels for the evaluation set")

    parser.add_argument("--testfile", type=str, required=True,
                        help="the bash file to perform the evaluation with (separately)")

    parser.add_argument("--identifier", type=str, required=True,
                        help="The identifier for this experiment to be used for the TREC CAsT challenge submission")
    parser.add_argument("--train_query_text_file", type=str, help="the file containing the training set dialogues")
    parser.add_argument("--eval_query_text_file", type=str, help="the file containing the evaluation set dialogues")
    parser.add_argument("--anserini_index_path", type=str,
                        default='/ivi/ilps/projects/Trec_CAST_2019/anserini_indexes/Joined/Joined_index',
                        help="the path to the anserini index of the collection(s)")
    parser.add_argument("--num_neg", type=int, default=1, help='# negative samples for training')
    parser.add_argument("--bert_ranker_name", type=str, default='vanilla',
                        help="ranker type to use for the CEDR-KNRM BERT model")
    parser.add_argument("--bert_dropout_prob", type=float, default=0.2,
                        help="The dropout probability to use for the bert model")

    parser.add_argument('--pretrain_marco', dest="pretrain_marco", action="store_true")
    parser.add_argument("--max_train_num_docs", type=int, help='only used in pretraining')
    parser.add_argument("--max_eval_num_docs", type=int, default=-1, help='only used in pretraining')
    parser.add_argument("--max_eval_num_queries", type=int, default=500, required=True)
    parser.add_argument("--embedding_drop_prob", type=float, default=0.8, help='Used for the representation embeddings')

    parser.add_argument("--resume_from", type=str, help="Specify epoch file to resume from")
    parser.add_argument("--continue_training_from_exp", type=str,
                        help="Specify experiment name to resume training from experiment or create a new one")
    parser.add_argument("--eval_use_cuda", dest="eval_use_cuda", action='store_true',
                        help="whether to use the GPU during evaluation")
    parser.add_argument("--no_distributed_launch", dest="distributed_launch", action='store_false',
                        help="whether using distributed launch")

    parser.add_argument("--do_global_layer_norm", dest="global_do_layer_norm", action='store_true',
                        help="Whether using global features on top of the BERT output")
    parser.add_argument("--no_scrict_weights_loading", dest="scrict_weights_loading", action='store_false',
                        help="Whether to load pre-trained weights by scrict matching or not")
    parser.add_argument("--global_mask_q_turn", type=int,
                        help="Number of turns to include as embeddings to the linear layer on top of bert")
    parser.add_argument("--do_normalize_initial_score", dest="do_normalize_initial_score", action='store_true',
                        help="whether to normalise the initial score of the unsupervised model ranking")
    parser.add_argument("--do_cap_to_top_docs", type=int, default=-1,
                        help="number of documents to use for ranking. -1 indicates to use all available documents")

    parser.add_argument("--use_different_BERT_lr", dest="use_different_BERT_lr", action='store_true',
                        help="whether to use different learning rate for the BERT weights")
    parser.add_argument("--Bert_lr", type=float, default=3e-6, help="The learning rate for the BERT model weights")

    parser.add_argument("--use_bert_Adam", dest="use_bert_Adam", action='store_true',
                        help="Whether to use the Adam optimiser from the pytorch-transformers library")
    parser.add_argument("--combine_normalised_scores", dest="combine_normalised_scores", action='store_true',
                        help="whether to combine the normalised scores with the unnormalised ones")

    parser.add_argument("--drop_plus_prob", type=float,
                        help="the number of weights to be sampled for the partially disabled learning")
    parser.add_argument("--train_global_QUAC", dest="train_global_QUAC", action='store_true',
                        help="Whether to use the global features for the pre-training on the QuAC dataset")
    parser.add_argument("--is_eval_QUAC", dest="is_eval_QUAC", action='store_true',
                        help="Whether the evaluation is done on the QuAC evaluation set")

    args = parser.parse_args()
    # Split metrics if more than one
    args.metrics = set(args.metrics.split(','))

    if not args.pretrain_marco:
        assert args.train_qrels_file is not None
        assert args.train_query_text_file is not None
        assert args.eval_query_text_file is not None

    t0 = time.time()
    train(args)

    print("\n-------------- Done in {0:.4f} [hours]---------------".format((time.time() - t0)/(60*60)))


if __name__ == '__main__':
    main()
