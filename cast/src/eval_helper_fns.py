import torch
import time
import random

from dataset.tools import un_correct_qid
from utils import dict_to_cuda, eval_metrics

from collections import defaultdict
import numpy as np
import operator
import json
from train_helper_fns import initialize_dataset_pretraining, initialize_dataset, initialize_dataloader


def evaluate_model(dataloader, eval_dataset, model, metrics, using_cuda, eval_losses, is_QUAC=False):
    """
    Evaluates the trained model, at a certain epoch, by generating the run file and calculating the metrics' scores
    :param dataloader: the dataloader with the evaluation data
    :param eval_dataset: the evaluation dataset
    :param model: the trained model
    :param metrics: the metrics to score
    :param using_cuda: whether to use the GPU
    :param eval_losses: the evaluation losses list
    :param is_QUAC: whether it is evaluating on the QuAC data
    :return: the runs dictionary and the metrics' scores
    """
    model.eval()
    runs = defaultdict(dict)
    with torch.no_grad():
        print("In total {} number of batches".format(len(dataloader)))
        for batch_id, batch in enumerate(dataloader, 0):
            if using_cuda:
                batch = dict_to_cuda(batch)

            q_ids = batch['qid'].view(-1)
            doc_ids = batch['docid'].view(-1)

            # Calculate ranking scores and save them in the run dictionary
            scores = model(batch, False).view(-1)
            for i in range(len(q_ids)):
                q_id = eval_dataset.get_real_q_id(q_ids[i].item())
                d_id = eval_dataset.get_real_p_id(doc_ids[i].item())
                score = scores[i].item()
                runs[un_correct_qid(q_id, is_QUAC=is_QUAC)][d_id] = score

        # Calculate average metrics' scores
        qrels_dict = eval_dataset.get_qrels_dict()
        mean_metrics_dict, _ = eval_metrics(qrels_dict, runs, metrics)

        eval_loss = -1  # Dummy loss. TODO calculate proper eval loss
        mean_metrics_dict['loss'] = eval_loss

    eval_losses.append(mean_metrics_dict['loss'])
    return mean_metrics_dict, runs


def generate_trec_cast_file_from_model_run(run, run_output_filename, max_docs, identifier, pretrain_marco=False):
    """
    generate the runs in trec cast format given the runs dictionary
    """
    print("Run file in trec cast format: {}".format(run_output_filename))

    run_out_file = open(run_output_filename, "w")

    for qid, doc_scores in run.items():
        trec_cast_run = defaultdict()
        if not pretrain_marco:
            topic, turn = qid.split('_')
        else:
            topic, turn = qid, ""

        # Get list of scores
        for indx, passage_score in enumerate(doc_scores.items()):
            if not pretrain_marco:
                collection, p_id = passage_score[0].split('_')
            else:
                collection, p_id = "MARCO", passage_score[0]
            collection = collection.lower()
            score = passage_score[1]
            entry = topic + "_" + turn + " Q0 " + collection.upper() + "_" + p_id
            trec_cast_run[entry] = np.float(score)

            if indx + 1 >= max_docs and not max_docs == -1:
                break

        # Sort them in descending order
        trec_cast_run = sorted(trec_cast_run.items(), key=operator.itemgetter(1), reverse=True)
        for rank, entry_items in enumerate(trec_cast_run):
            entry_key = entry_items[0]
            entry_score = entry_items[1]
            entry = entry_key + " " + str(rank + 1) + " " + str(entry_score) + " " + identifier

            run_out_file.write(entry)

            run_out_file.write("\n")
            if rank + 1 >= max_docs and not max_docs == -1:
                break
    run_out_file.close()


def generate_fake_qrels_file(args):
    """
    Generates fake qrels file for the TEST set which does not have qrels
    TODO: do not require a fake qrels file
    """
    tmp_file_name = './tmp_fake_qrels.json'
    with open(tmp_file_name, 'w') as output_qrels:
        fake_qrels_dict = defaultdict(dict)
        for line in open(args.run_file, 'r'):
            qid, _, docid, rank, score, _ = line.split()
            fake_qrels_dict[qid][docid] = random.randint(0, 2)
        json.dump(fake_qrels_dict, output_qrels)
    return tmp_file_name


def get_eval_score_and_runs(pretrain_marco, tokenizer, using_cuda, run_file, max_docs, qrels, batch_size,
                            num_workers, model, metrics, max_num_queries=None, eval_query_text_file=None,
                            anserini_index_path=None, do_normalize_initial_score=False,
                            combine_normalised_scores=False,
                            is_QUAC=False):
    """
    Loads the dataset, and dataloaders, performs the evaluation pass through the network and returns evaluation result
    :param pretrain_marco: whether using MS MARCO data
    :param tokenizer: the pre-trained tokeniser
    :param using_cuda: whether using the GPU
    :param run_file: the runs file to use
    :param max_docs: the number of maximum documets to score
    :param qrels: the file containing the qrels
    :param batch_size: the batch size for the model pass
    :param num_workers: the number of workers to use for dataloading
    :param model: the trained model at a certain epoch
    :param metrics: the metrics to use for scoring
    :param max_num_queries: the maximum number of queries to use for evaluation.
    More means more aacurate scores but takes longer
    :param eval_query_text_file: the dialogues file
    :param anserini_index_path: the path to the anserini index of the collections
    :param do_normalize_initial_score: whether to normalise the initial score from the unsupervised language model run
    :param combine_normalised_scores: whether to combine the un-normalised with the normalised scores
    :param is_QUAC: whether using the QuAC evaluation set
    :return: the evaluation results for the model
    """
    if pretrain_marco:
        eval_dataset = initialize_dataset_pretraining(tokenizer, using_cuda, is_training=False,
                                                      run_file=run_file, max_num_docs=max_docs,
                                                      max_num_queries=max_num_queries, eval_qrels=qrels)
    else:
        eval_dataset = initialize_dataset(tokenizer, using_cuda, qrels_file=qrels, run_file=run_file,
                                          query_text_file=eval_query_text_file,
                                          anserini_index_path=anserini_index_path, mode='eval',
                                          do_normalize_initial_score=do_normalize_initial_score,
                                          combine_normalised_scores=combine_normalised_scores)

    eval_dataloader = initialize_dataloader(eval_dataset, False, using_cuda, batch_size,
                                            num_workers=num_workers)

    return evaluate_model(eval_dataloader, eval_dataset, model, metrics, using_cuda, [], is_QUAC=is_QUAC)



