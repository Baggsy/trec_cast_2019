import argparse
import time
import os
import pickle
from train_helper_fns import send_command_if_file_EOF


def main():
    """
    Submits the evaluation jobs in sequence so that the cluster is not over loaded/blocked
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--check_list_filename", type=str, required=True)
    parser.add_argument("--evaluations_dir", type=str, required=True)
    parser.add_argument("--minutes_to_wait", type=int, default=3)
    parser.add_argument("--num_eval_at_a_time", type=int, default=15)
    args = parser.parse_args()

    print("Running evaluations for evaluations_dir: {}".format(args.evaluations_dir))
    cmds, evaluate_epochs_list = pickle.load(open(args.check_list_filename, "rb"))
    len_cmds = len(cmds)
    print(cmds)
    print(evaluate_epochs_list)

    for e in range(args.num_eval_at_a_time):
        if e >= len_cmds:
            break
        os.system(cmds[0])
        cmds.pop(0)

    while len(cmds) > 0:
        for epoch_at_a_time in range(min(args.num_eval_at_a_time, len(evaluate_epochs_list))):
            if len(cmds) == 0 or (epoch_at_a_time >= len(evaluate_epochs_list)):
                break
            check_file = os.path.join(args.evaluations_dir, "Epoch_{}_log.txt".format(evaluate_epochs_list[epoch_at_a_time]))
            cmds, evaluate_epochs_list, epoch_at_a_time = send_command_if_file_EOF(check_file, cmds, evaluate_epochs_list, queued_epoch=epoch_at_a_time)
            minutes_to_wait = args.minutes_to_wait
            if len(evaluate_epochs_list) == 0 or len(cmds) == 0:
                break
            print("Next check: Epoch_{}_log.txt".format(evaluate_epochs_list[epoch_at_a_time]))
        print("Waiting {} minutes".format(minutes_to_wait))
        time.sleep(minutes_to_wait * 60)

    os.remove(args.check_list_filename)


if __name__ == '__main__':
    main()
